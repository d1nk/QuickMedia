# QuickMedia
A rofi inspired native client for web services.
Currently supported web services: `youtube`, `peertube`, `lbry`, `soundcloud`, `nyaa.si`, `manganelo`, `manganelos`, `mangatown`, `mangakatana`, `mangadex`, `readm`, `onimanga`, `4chan`, `matrix`, `saucenao`, `hotexamples`, `anilist` and _others_.\
QuickMedia also supports reading local manga and watching local anime, see [local manga](#local-manga) and [local anime](#local-anime)
## Usage
```
usage: quickmedia [plugin] [--dir <directory>] [-e <window>] [youtube-url]
OPTIONS:
  plugin                       The plugin to use. Should be either launcher, 4chan, manga, manganelo, manganelos, mangatown, mangakatana, mangadex, readm, onimanga, local-manga, local-anime, youtube, peertube, lbry, soundcloud, nyaa.si, matrix, saucenao, hotexamples, anilist, file-manager or stdin
  --no-video                   Only play audio when playing a video. Disabled by default
  --upscale-images             Upscale low-resolution manga pages using waifu2x-ncnn-vulkan. Disabled by default
  --upscale-images-always      Upscale manga pages using waifu2x-ncnn-vulkan, no matter what the original image resolution is. Disabled by default
  --dir <directory>            Set the start directory when using file-manager
  --instance <instance>        The instance to use for peertube
  -e <window-id>               Embed QuickMedia into another window
  --video-max-height <height>  Media plugins will try to select a video source that is this size or smaller
EXAMPLES:
  quickmedia
  quickmedia --upscale-images-always manganelo
  quickmedia https://www.youtube.com/watch?v=jHg91NVHh3s
  echo -e "hello\nworld" | quickmedia stdin
  tabbed -c -k quickmedia launcher -e
```
## Installation
If you are running arch linux then you can install QuickMedia from aur (https://aur.archlinux.org/packages/quickmedia-git/), otherwise you will need to first install [sibs](https://git.dec05eba.com/sibs/) and then run `./install.sh` as root.
## Dependencies
### Libraries
`libglvnd (LibGL.so)`, `libx11`, `libxrandr`, `libmpv`
### Executables
`curl`
### Fonts
`noto-fonts` (when `use_system_fonts` config is not set to `true`)
### Optional
`noto-fonts-cjk` needs to be installed to view chinese, japanese and korean characters  (when `use_system_fonts` config is not set to `true`).\
`youtube-dl` needs to be installed to play/download xxx videos.\
`notify-send` (which is part of `libnotify`) needs to be installed to show notifications (on Linux and other systems that uses d-bus notification system).\
[automedia](https://git.dec05eba.com/AutoMedia/) needs to be installed when tracking manga with `Ctrl + T`.\
`waifu2x-ncnn-vulkan` needs to be installed when using the `--upscale-images` or `--upscale-images-always` option.\
`xdg-utils` which provides `xdg-open` needs to be installed when downloading torrents with `nyaa.si` plugin.\
`ffmpeg (and ffprobe which is included in ffmpeg)` needs to be installed to display webp thumbnails, to upload videos with thumbnails on matrix or to merge video and audio when downloading youtube videos.\
`fc-match` (which is part of `fontconfig`) needs to be installed when `use_system_fonts` config is set to `true`.
## Controls
### General control
Type text and then wait and QuickMedia will automatically search.\
`Enter`: Go to the page in the selected item.\
`Esc`: Go back to the previous page or close QuickMedia if there is no previous page (for all plugins except matrix).\
`Arrow up`/`Ctrl+K`: Move up.\
`Arrow down`/`Ctrl+J`: Move down.\
`Left`/`Ctrl+H`: Move left.\
`Right`/`Ctrl+L`: Move right.\
`Ctrl+Left`/`Ctrl+Alt+H`/`Ctrl+Shift+Tab`: Select the tab to the left.\
`Ctrl+Right`/`Ctrl+Alt+L`/`Ctrl+Tab`: Select the tab to the right.\
`Ctrl+1 to 9`: Select the specified tab.\
`Home`: Scroll to the top (the first item).\
`End`: Scroll to the bottom (the last item).\
`Page up`: Scroll up an entire page.\
`Page down`: Scroll down an entire page.\
`Ctrl+Q`: Close QuickMedia.\
`Ctrl+D`: Clear the input text.\
`Ctrl+C`: Copy the text in the selected item to your clipboard.\
`Ctrl+V`: Paste clipboard content into the search bar.\
`Ctrl+I`: Select which url in the selected item to open in a browser.\
`Tab`: Set search input to the title of the selected item.\
`Ctrl+Enter`: Submit search ignoring the selected item. Only works for matrix room directory page and matrix invites page.
### Video controls
`mpv` controls apply in general, see https://mpv.io/manual/master/#interactive-control.\
`Esc`/`Backspace`/`Q`: Close the video.\
`Ctrl+F`: Toggle between fullscreen mode and window mode. `Esc` can also be used to switch from fullscreen mode to window mode.\
`Ctrl+R`: Show pages related to the video, such as comments, related videos or channel videos (if supported).\
`Ctrl+S`: Save the video/music.\
`Ctrl+C`: Copy the url of the currently playing video to the clipboard (with timestamp, if supported).\
`F5`: Reload the video/music.
### Youtube channel controls
`Ctrl+T`: Subscribe/Unsubscribe from the channel.
### Manga search/history/chapters page controls
`Ctrl+B`: Bookmark the selected manga. If the manga is already bookmarked then its removed from bookmarks.
### Local manga search page controls
`Ctrl+R`: Mark the manga as read/unread.
### Local anime search page controls
`Ctrl+R`: Mark the anime as watched/unwatched.
### Manga page view controls
`Arrow up`/`Ctrl+K`: Go to the next page (or chapter if the current page is the last one).\
`Arrow down`/`Ctrl+J`: Go to the previous page (or chapter if the current page is the first one).\
`Page up`: Go back 10 pages.\
`Page down`: Go forward 10 pages.\
`Home`: Go to the first page.\
`End`: Go to the last page.\
`F`: Toggle between scaling the image to the window size or only down scaling if the image is too large.\
`I`: Switch to scroll view.
### Manga scroll view controls
`Arrow up`/`Ctrl+K`: Move up.\
`Arrow down`/`Ctrl+J`: Move down.\
`Mouse scroll`/`Middle mouse click+Move mouse`: Smoothly scroll.\
`F`: Toggle between scaling the image to the window size or only down scaling if the image is too large.\
`I`: Switch to page view.
### Manga chapters controls
`Ctrl+T`: Start tracking the manga after the selected chapter ([AutoMedia](https://git.dec05eba.com/AutoMedia/) needs to be installed).
### Matrix controls
#### Login page controls
`Tab`: Switch between username, password and homeserver fields.\
`Enter`: Login.
#### Chat page controls
`I`: Start typing a message, `Esc` to cancel.\
`R`: Start replying to the selected message, `Esc` to cancel.\
`E`: Edit the selected message (if it was posted by you), `Esc` to cancel.\
`Ctrl+D`: Delete the selected message (if it was posted by you).\
`Enter`: View image/video attached to the message, or if its a file then download it. Brings up a list of urls in the selected item if there are any.\
`Ctrl+I`: Reverse image search the selected image.\
`Ctrl+S`: Save the selected file.\
`Ctrl+V`: Uploads the file specified in the clipboard.\
`U`: Bring up the file manager and select a file to upload to the room, `Esc` to cancel.\
`Alt+Up`/`Ctrl+Alt+K`: Select the room above the currently selected room.\
`Alt+Down`/`Ctrl+Alt+J`: Select the room below the currently selected room.\
`Alt+Home`: Select the first room in the room list.\
`Alt+End`: Select the last room in the room list.\
`Ctrl+P`: Pin the selected message.\
`Ctrl+R`: Navigate to the replied to message.\
`Ctrl+B`: Navigate to the bottom (the latest message).
#### Pinned messages page controls
`Ctrl+D`: Unpin the selected message.\
`Ctrl+R`: Navigate to the pinned message in the messages tab.
#### Message input controls
`Esc`: Stop typing (also clears the input text).\
`Enter`: Post message.\
`Arrow up`: Go to the previous line.\
`Arrow down`: Go to the next line.\
`Left`: Go to the previous character.\
`Right`: Go to the next character.\
`Ctrl+Left`: Go to the previous word.\
`Ctrl+Right`: Go to the next word.\
`Ctrl+V`: Paste clipboard content into the message input.\
`Ctrl+D`: Clear the input text.\
`@`: Start searching for a user to mention.
### 4chan thread controls
`Enter`: Show the posts that the selected post replies to and the posts the replied to the selected post.\
`Backspace`: Go back to the previously selected item after selecting it with `Enter`.\
`P`: View image/video attached to the post. If the thread is a thread with videos, then all videos are played starting from the selected one.\
`I`: Start typing a message, `Esc` to cancel.\
`Enter`: Post message (when typing a message).\
`R`: Add the selected post as a reply to the message.\
`U`: Select a file to upload by selecting a file with the file manager, `Esc` to cancel.\
`Ctrl+D`: Remove the selected file from the post.\
`Ctrl+I`: Reverse image search the selected image or select an url to open in the browser.\
`Arrow left/right`: Move captcha slider left/right.\
`Ctrl+S`: Save the image/video attached to the selected post.
### 4chan image controls
`Mouse scroll`: Zoom.\
`Left mouse button + move`: Pan (move) the image.\
`W`: Reset zoom and panning, scaling image to window size.
### File save controls
`Tab`: Switch between navigating the file manager and file name.\
`Ctrl+Enter`/`Click on save`: Save the file.\
`Esc`/`Click on cancel`: Cancel download.
## Matrix text commands
`/help`: Show a list of valid commands.\
`/upload`: Bring up the file manager and select a file to upload to the room, `Esc` to cancel.\
`/join [room]`: Join a room by name or id.\
`/invite`: Invite a user to the room.\
`/logout`: Logout.\
`/leave`: Leave the current room.\
`/me [text]`: Send a message of type "m.emote".\
`/react [text]`: React to the selected message (also works if you are replying to a message).\
`/id`: Show the room id.
## Config
Config is loaded from `~/.config/quickmedia/config.json` if it exists. See [example-config.json](https://git.dec05eba.com/QuickMedia/plain/example-config.json) for an example config. All fields in the config file are optional.\
If `use_system_mpv_config` is set to `true` then your systems mpv config in `~/.config/mpv/mpv.conf` and plugins will be used.
## Theme
Theme is loaded from `~/.config/quickmedia/themes/<theme-name>.json` if it exists or from `/usr/share/quickmedia/themes`. Theme name is set in `~/.config/quickmedia/config.json` under the variable `theme`.\
Default themes available: `default, nord`.\
See [default.json](https://git.dec05eba.com/QuickMedia/plain/themes/default.json) for an example theme.\
The `default` is used by default.
## <a id="local-manga"></a>Local manga
`local_manga.directory` needs to be set in `~/.config/quickmedia/config.json` to a directory with all your manga.\
The directory layout is expected to be like this:
```
Manga 1
    Chapter 1
        1.png
        2.png
    Chapter 2
        1.png
        2.png
Manga 2
    Chapter 1
        1.png
        2.png
    Chapter 2
        1.png
        2.png
```
Note that the manga name and chapter names can be anything you want, but the image names need to be a number (with `.jpg`, `.jpeg`, or `.png` extension).\
It's ok to use the same directory for local manga and local anime.
## <a id="local-anime"></a>Local anime
`local_anime.directory` needs to be set in `~/.config/quickmedia/config.json` to a directory with all your anime.\
The directory layout is expected to be like this:
```
Anime 1 - Episode 1.mkv
Anime 1 - Episode 2.mkv
Anime 2
    Episode 1.mkv
    Episode 2.mkv
Anime 3
    Season 1
        Episode 1.mkv
        Episode 2.mkv
    Season 2
        Episode 1.mkv
        Episode 2.mkv
```
Note that the episode names can be anything you want.\
Local anime always uses your system mpv config.\
It's ok to use the same directory for local manga and local anime.
## Environment variables
If `xdg-open` is not installed then the `BROWSER` environment variable is used to open links in a browser.\
Set `QM_PHONE_FACTOR=1` to disable the room list side panel in matrix.
## UI scaling
Set `GDK_SCALE` environment variable or add `Xft.dpi` to `$HOME/.Xresources`. For example a value of 96 for the `Xft.dpi` means 1.0 scaling and 144 (96*1.5) means 1.5 scaling.\
Note that at the moment, images do also not scale above their original size.\
Scaling can also be set in `~/.config/quickmedia/config.json`, which will override `GDK_SCALE` and `$HOME/.Xresources` `Xft.dpi`.
## Storage
Config data, including manga progress is stored under `$XDG_CONFIG_HOME/quickmedia` or `~/.config/quickmedia`.\
Cache is stored under `$XDG_CACHE_HOME/quickmedia` or `~/.cache/quickmedia`.
## Tabs
[tabbed](https://tools.suckless.org/tabbed/) can be used to put quickmedia windows into tabs. After installing `tabbed`, run `tabbed -c -k quickmedia launcher -e`.
## License
QuickMedia is free software licensed under GPL 3.0, see LICENSE for more details. `images/emoji.png` uses Noto Color Emoji, which is licensed under the Apache license, see: https://github.com/googlefonts/noto-emoji.
# Screenshots
## Youtube search
![](https://dec05eba.com/images/youtube.webp)
## Youtube video
![](https://dec05eba.com/images/youtube-video.webp)
## Youtube comments
![](https://dec05eba.com/images/youtube-comments.webp)
## Manganelo search
![](https://dec05eba.com/images/manganelo.webp)
## Manganelo chapters
![](https://dec05eba.com/images/manganelo-chapters.webp)
## Manganelo page
![](https://dec05eba.com/images/manganelo-page.webp)
## 4chan thread
![](https://dec05eba.com/images/4chan.webp)
## Matrix chat
![](https://dec05eba.com/images/matrix.webp)
