#!/bin/sh -e

[ $# -ne 1 ] && echo "usage: build.sh debug|release" && exit 1

script_dir=$(dirname "$0")
cd "$script_dir"

sibs build --"$1" video_player
sibs build --"$1"
echo "Successfully built the video player and QuickMedia"
