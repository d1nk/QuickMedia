#!/bin/sh -e

create_header_file() {
    echo "#pragma once

#include <string>

// This file was automatically generated with generate-tld.sh, do not edit manually!

namespace QuickMedia {
    bool is_tld(const std::string &str);
}
"
}

create_source_file() {
    echo "#include \"Tlds.hpp\"
#include <unordered_set>

// This file was automatically generated with generate-tld.sh, do not edit manually!

namespace QuickMedia {
    // Source: https://data.iana.org/TLD/tlds-alpha-by-domain.txt
    static const std::unordered_set<std::string> TLDS = {"
    curl -sfL https://data.iana.org/TLD/tlds-alpha-by-domain.txt | grep -v '^#' | tr '[:upper:]' '[:lower:]' | awk '{ printf "        \"%s\",\n", $1 }'
    echo "    };
"

    echo "    bool is_tld(const std::string &str) {
        return TLDS.find(str) != TLDS.end();
    }
}"
}

create_header_file > generated/Tlds.hpp
create_source_file > generated/Tlds.cpp