#pragma once

#include <string>

// This file was automatically generated with generate-tld.sh, do not edit manually!

namespace QuickMedia {
    bool is_tld(const std::string &str);
}

