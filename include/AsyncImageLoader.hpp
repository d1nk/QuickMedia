#pragma once

#include "../include/Storage.hpp"
#include "../include/MessageQueue.hpp"
#include "../include/FileAnalyzer.hpp"
#include "../include/AsyncTask.hpp"
#include <mglpp/system/vec.hpp>
#include <mglpp/graphics/Texture.hpp>
#include <mglpp/graphics/Image.hpp>
#include <mglpp/system/Clock.hpp>
#include <string>
#include <memory>
#include <unordered_map>

namespace QuickMedia {
    enum class LoadingState {
        NOT_LOADED,
        LOADING,
        READY_TO_LOAD,
        FINISHED_LOADING,
        APPLIED_TO_TEXTURE
    };

    struct ThumbnailData {
        LoadingState loading_state = LoadingState::NOT_LOADED;
        mgl::Texture texture;
        std::unique_ptr<mgl::Image> image; // Set in another thread. This should be .reset after loading it into |texture|, to save memory
        size_t counter = 0;
        Path thumbnail_path;
    };

    struct ThumbnailLoadData {
        Path path;
        Path thumbnail_path;
        bool local;
        std::shared_ptr<ThumbnailData> thumbnail_data;
        mgl::vec2i resize_target_size;
    };

    // If |symlink_if_no_resize| is false then a copy is made from |thumbnail_path| to |thumbnail_path_resized| instead of a symlink if |thumbnail_path| is not larger than |resize_target_size|.
    // One example of why you might not want a symlink is if |thumbnail_path| is a temporary file.
    bool create_thumbnail(const Path &thumbnail_path, const Path &thumbnail_path_resized, mgl::vec2i resize_target_size, ContentType content_type, bool symlink_if_no_resize);

    constexpr int NUM_IMAGE_DOWNLOAD_PARALLEL = 4;
    constexpr int NUM_IMAGE_LOAD_PARALLEL = 4;

    class AsyncImageLoader {
    public:
        static AsyncImageLoader& get_instance();

        // Never returns nullptr. Instead check the |loading_state| of the thumbnail data to see if it has finished loading.
        // This function should be called every frame for the objects that need to display this thumbnail, otherwise it can be unloaded.
        // set |resize_target_size| to {0, 0} to disable resizing.
        // Note: this method is not thread-safe
        std::shared_ptr<ThumbnailData> get_thumbnail(const std::string &url, bool local, mgl::vec2i resize_target_size);

        // Note: this should only be called once every frame.
        // Note: this method is not thread-safe
        void update();
    private:
        struct Download {
            ReadProgram read_program;
            int64_t download_start = 0;
            Path thumbnail_path;
            std::shared_ptr<ThumbnailData> thumbnail_data;
            mgl::vec2i resize_target_size;
            std::string url;
        };

        AsyncImageLoader();
        ~AsyncImageLoader();
        AsyncImageLoader(AsyncImageLoader &other) = delete;
        AsyncImageLoader& operator=(AsyncImageLoader &other) = delete;

        // set |resize_target_size| to {0, 0} to disable resizing.
        // Note: this method is not thread-safe
        bool load_thumbnail(const std::string &url, bool local, mgl::vec2i resize_target_size, std::shared_ptr<ThumbnailData> thumbnail_data, Path &thumbnail_path);

        // Returns -1 if all threads are busy
        int get_free_load_index() const;

        void load_create_thumbnail(const Path &thumbnail_path, const Path &thumbnail_path_resized, ThumbnailData *thumbnail_data, mgl::vec2i resize_target_size);
        void process_thumbnail(ThumbnailLoadData &thumbnail_load_data);
    private:
        void reset_download(Download &download);
    private:
        std::mutex download_mutex;
        // TODO: Use curl single-threaded multi-download feature instead
        Download downloads[NUM_IMAGE_DOWNLOAD_PARALLEL];
        AsyncTask<void> load_threads[NUM_IMAGE_LOAD_PARALLEL];
        MessageQueue<ThumbnailLoadData> image_thumbnail_create_queue;
        std::unordered_map<std::string, std::shared_ptr<ThumbnailData>> thumbnails;
        size_t counter = 0;
    };
}
