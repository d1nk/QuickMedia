#pragma once

#include "BodyItem.hpp"
#include "RoundedRectangle.hpp"
#include <mglpp/graphics/Rectangle.hpp>
#include <mglpp/graphics/Sprite.hpp>
#include <functional>

namespace mgl {
    class Window;
    class Shader;
    class Texture;
    class Event;
}

namespace Json {
    class Value;
}

namespace QuickMedia {
    void init_body_themes();

    class ThumbnailData;
    using BodyItemRenderCallback = std::function<void(std::shared_ptr<BodyItem> &body_item)>;
    // Return true to merge
    using BodyItemMergeHandler = std::function<bool(BodyItem *prev_item, BodyItem *this_item)>;

    class BodyItemList {
    public:
        BodyItemList(BodyItems *body_items) : body_items(body_items) {}
        std::shared_ptr<BodyItem>* data() { return body_items->data(); }
        const std::shared_ptr<BodyItem>* data() const { return body_items->data(); }
        std::shared_ptr<BodyItem>& operator[](size_t index) { return (*body_items)[index]; }
        const std::shared_ptr<BodyItem>& operator[](size_t index) const { return (*body_items)[index]; }
        size_t size() const { return body_items->size(); }
    private:
        BodyItems *body_items;
    };

    enum class AttachSide {
        TOP,
        BOTTOM
    };

    class Body {
    public:
        Body(BodyTheme body_theme, mgl::Texture &loading_icon_texture, mgl::Shader *rounded_rectangle_shader, mgl::Shader *rounded_rectangle_mask_shader);
        ~Body();

        // Select previous page, ignoring invisible items. Returns true if the item was changed. This can be used to check if the top was hit when wrap_around is set to false
        bool select_previous_page();
        // Select next page, ignoring invisible items. Returns true if the item was changed. This can be used to check if the bottom was hit when wrap_around is set to false
        bool select_next_page();

        // Select previous item, ignoring invisible items. Returns true if the item was changed or if the item scrolled. This can be used to check if the top was hit when wrap_around is set to false
        bool select_previous_item(bool scroll_page_if_large_item = false, bool ignore_columns = false);

        // Select next item, ignoring invisible items. Returns true if the item was changed or if the item scrolled. This can be used to check if the bottom was hit when wrap_around is set to false
        bool select_next_item(bool scroll_page_if_large_item = false, bool ignore_columns = false);
        void set_selected_item(int item, bool reset_prev_selected_item = true);
        void set_selected_item(BodyItem *body_item);
        void reset_prev_selected_item();

        // Returns -1 if item can't be found
        int get_index_by_body_item(BodyItem *body_item);
        
        void select_first_item(bool reset_page_scroll = true);
        void select_last_item();
        void set_items(BodyItems items);
        void clear_items();
        void prepend_item(std::shared_ptr<BodyItem> body_item);
        void prepend_items_reverse(BodyItems new_items);
        void append_item(std::shared_ptr<BodyItem> body_item);
        void append_items(BodyItems new_items);
        void insert_item(std::shared_ptr<BodyItem> body_item, int index);
        void move_item(size_t src_index, size_t dst_index);
        // Returns the inserted position
        size_t insert_item_by_timestamp_reverse(std::shared_ptr<BodyItem> body_item);
        // Returns the inserted position
        size_t insert_item_by_timestamp(std::shared_ptr<BodyItem> body_item);
        // Note: keeps the selected item (moving the selected_item index if necessary)
        void insert_items_by_timestamps(BodyItems new_items);
        void for_each_item(std::function<void(std::shared_ptr<BodyItem>&)> callback);
        // Return true if the item is the match
        std::shared_ptr<BodyItem> find_item(std::function<bool(std::shared_ptr<BodyItem>&)> callback);
        // Returns -1 if no item is found
        int find_item_index(std::function<bool(std::shared_ptr<BodyItem>&)> callback);
        // Return true to remove the current item.
        // Returns true if the item was found.
        // Note: only removes the first item found.
        bool erase_item(std::function<bool(std::shared_ptr<BodyItem>&)> callback);
        std::shared_ptr<BodyItem> get_item_by_index(size_t index);
        BodyItemList get_items();
        BodyItems get_items_copy();
        // If |end_index| is (size_t)-1, then all items starting from |start_index| until the end are copied
        void copy_range(size_t start_index, size_t end_index, BodyItems &target);
        size_t get_num_items() const;
        void reverse_items();
        void clear_cache();
        void clear_text_cache();

        BodyItem* get_selected() const;
        std::shared_ptr<BodyItem> get_selected_shared();

        void clamp_selection();

        // Returns true if the event was handled
        bool on_event(const mgl::Window &window, const mgl::Event &event, bool keyboard_navigation = true);
        // Returns the the body total height
        double draw(mgl::Window &window, mgl::vec2f pos, mgl::vec2f size);
        // Returns the the body total height
        double draw(mgl::Window &window, mgl::vec2f pos, mgl::vec2f size, const Json::Value &content_progress);
        // |size| is the clip size, another outside this will be cut off.
        // Note: this should be called after |draw|, or thumbnails will be messed up. TODO: find a way to solve this issue in a clean way.
        // This happens because of |draw| sets thumbnails as unreferenced at the beginning and cleans them up at the end if they are not drawn in the same function call.
        // TODO: Right now drawing an item that also exists in the body will cause the text to update geometry every frame if the text is wrapping text and the items are drawn at different sizes,
        // because of Text::setMaxWidth
        void draw_item(mgl::Window &window, std::shared_ptr<BodyItem> &item, mgl::vec2f pos, mgl::vec2f size, bool include_embedded_item = true, bool is_embedded = false);

        float get_item_height(BodyItem *item, float width, bool load_texture = true, bool include_embedded_item = true, bool merge_with_previous = false, int item_index = -1);

        // TODO: Highlight the part of the text that matches the search.
        void filter_search_fuzzy(const std::string &text);

        int get_selected_item() const { return selected_item; }
        bool is_selected_item_last_visible_item() const;

        void set_page_scroll(float scroll);
        float get_page_scroll() const { return page_scroll; }
        // This is the item we can see the start of
        bool is_top_cut_off() const { return top_cut_off; }
        // This is the item we can see the end of
        bool is_bottom_cut_off() const { return bottom_cut_off; }
        int get_num_visible_items() const { return num_visible_items; };
        
        void apply_search_filter_for_item(BodyItem *body_item);

        bool can_move_left() const { return selected_column > 0; }
        bool can_move_right() const { return selected_column + 1 < num_columns; }

        bool draw_thumbnails;
        // Set to {0, 0} to disable resizing
        mgl::vec2i thumbnail_max_size;
        BodyItemRenderCallback body_item_render_callback;
        BodyItemMergeHandler body_item_merge_handler;
        std::function<void(BodyItem*)> body_item_select_callback;
        mgl::Shader *thumbnail_mask_shader;
        AttachSide attach_side = AttachSide::TOP;
        bool title_mark_urls = false;
        bool swiping_enabled = false;
        bool show_drop_shadow = true;
        bool card_view = false;

        std::function<void()> on_top_reached = nullptr;
        std::function<void()> on_bottom_reached = nullptr;
    private:
        void draw_drop_shadow(mgl::Window &window);
        void filter_search_fuzzy_item(const std::string &text, BodyItem *body_item);
        void handle_item_render(const mgl::vec2f pos, const float item_width, const float item_height, int item_index);
        // Returns the the body total height
        double draw_list_view(mgl::Window &window, mgl::vec2f pos, mgl::vec2f size, const Json::Value &content_progress);
        // Returns the the body total height
        double draw_card_view(mgl::Window &window, mgl::vec2f pos, mgl::vec2f size, mgl::vec2i window_size, float scissor_y, bool &switched_to_list_view);
        void draw_item(mgl::Window &window, std::shared_ptr<BodyItem> &item, const mgl::vec2f &pos, const mgl::vec2f &size, const float item_height, const int item_index, const Json::Value &content_progress, bool include_embedded_item = true, bool merge_with_previous = false);
        void draw_card_item(mgl::Window &window, std::shared_ptr<BodyItem> &item, const mgl::vec2f &pos, const mgl::vec2f &pos_offset, const mgl::vec2f &body_size, const mgl::vec2f &window_size, float item_height, float scissor_y, int item_index, ThumbnailData *item_thumbnail);
        void update_dirty_state(BodyItem *body_item, float width);
        void clear_body_item_cache(BodyItem *body_item);
        mgl::vec2i get_item_thumbnail_size(BodyItem *item) const;

        // Returns -1 if not found
        int get_previous_visible_item(int start_index);
        // Returns -1 if not found
        int get_next_visible_item(int start_index);
    private:
        enum class DirtyState {
            FALSE,
            TRUE,
            FORCE_TRUE
        };

        enum class TargetSetState {
            NOT_SET,
            SET,
            APPLIED
        };

        enum class StuckDirection {
            TOP,
            NONE,
            BOTTOM
        };

        struct RenderItem {
            std::shared_ptr<BodyItem> body_item;
            mgl::vec2f pos;
            mgl::vec2f pos_offset;
            mgl::vec2f size;
            float item_height;
            int item_index;
            bool merge_with_previous;
            std::shared_ptr<ThumbnailData> item_thumbnail;
        };

        // TODO: Prevent items from being removed when render is in progress. That would invalidate references and cause a crash
        BodyItems items;

        BodyTheme body_theme;

        // Items that are rendered in the current frame
        std::vector<RenderItem> render_items;

        int selected_item;
        int prev_selected_item;
        double page_scroll = 0.0;
        float selected_scrolled = 0.0f;
        // TODO: Use a loading gif or something similar instead, to properly indicate the image is loading. Which could also have text that says "failed to load image" when image loading failed.
        // TODO: Use rounded rectangle instead
        mgl::Rectangle image_fallback;
        //mgl::Rectangle item_separator;
        mgl::Sprite image;
        mgl::Sprite loading_icon;

        mgl::Text progress_text;
        mgl::Text replies_text;
        mgl::Text embedded_item_load_text;

        bool body_size_changed = false;

        int num_visible_items;
        bool top_cut_off;
        bool bottom_cut_off;
        int first_visible_item = -1;
        int last_visible_item = -1;
        mgl::Clock draw_timer;
        mgl::Clock frame_timer;
        double elapsed_time_sec = 0.0;
        bool selected_line_top_visible = true;
        bool selected_line_bottom_visible = true;
        bool selected_item_fits_in_body = true;
        bool mouse_left_pressed = false;
        bool mouse_left_clicked = false;
        bool has_scrolled_with_input = false;
        bool click_counts = false;
        int num_columns = 1;
        int selected_column = 0;
        bool card_view_enabled = true;
        StuckDirection stuck_direction = StuckDirection::NONE;
        mgl::vec2f mouse_click_pos;
        mgl::vec2f mouse_release_pos;
        double mouse_press_pixels_moved_abs;
        mgl::vec2f mouse_pos;
        mgl::vec2i prev_mouse_pos_raw;
        mgl::vec2i mouse_pos_raw;
        mgl::vec2f mouse_scroll_accel;
        mgl::vec2f body_pos;
        mgl::vec2f body_size;
        double body_swipe_x = 0.0;
        bool body_swipe_move_right = false;
        bool grabbed_left_side = false;
        float selected_item_height = 0.0f;
        std::shared_ptr<BodyItem> clicked_body_item = nullptr;
        RoundedRectangle item_background;
        RoundedRectangle reaction_background;
        bool render_selected_item_bg = true;
        mgl::vec2f item_background_prev_pos;
        mgl::vec2f item_background_target_pos;
        mgl::vec2f item_background_prev_size;
        mgl::vec2f item_background_target_size;
        float item_background_target_height = 0.0f;
        TargetSetState target_set = TargetSetState::NOT_SET;
        std::string current_filter;
        bool using_filter = false;
        mgl::Shader *rounded_rectangle_shader;
        mgl::Shader *rounded_rectangle_mask_shader;
    };
}