#pragma once

#include <string>

namespace QuickMedia {
    struct SearchConfig {
        int font_size = 16;
    };

    struct TabConfig {
        int font_size = 16;
    };

    struct BodyConfig {
        int title_font_size = 16;
        int author_font_size = 14;
        int description_font_size = 14;
        int timestamp_font_size = 10;
        int reaction_font_size = 14;
        int progress_font_size = 14;
        int replies_font_size = 14;
        int embedded_load_font_size = 14;
    };

    struct InputConfig {
        int font_size = 16;
    };

    struct VideoConfig {
        int max_height = 0;
    };

    struct LocalMangaConfig {
        std::string directory;
        bool sort_by_name = false;
        bool sort_chapters_by_name = false;
    };

    struct LocalAnimeConfig {
        std::string directory;
        bool sort_by_name = false;
        bool auto_group_episodes = false;
    };

    struct Config {
        Config() = default;
        Config(const Config&) = delete;
        Config&operator=(const Config&) = delete;

        SearchConfig search;
        TabConfig tab;
        BodyConfig body;
        InputConfig input;
        VideoConfig video;
        LocalMangaConfig local_manga;
        LocalAnimeConfig local_anime;
        bool use_system_fonts = false;
        bool use_system_mpv_config = false;
        std::string theme = "default";
        float scale = 1.0f;
        float font_scale = 1.0f;
        float spacing_scale = 1.0f;
    };

    const Config& get_config();
}