#pragma once

#include "Path.hpp"
#include <string>
#include <vector>
#include <functional>
#include <rapidjson/fwd.h>

namespace QuickMedia {
    enum class DownloadResult {
        OK,
        ERR,
        NET_ERR
    };

    struct CommandArg {
        std::string option;
        std::string value; // Optional
    };

    // Return true the return DownloadResult::OK for the download, which also saves the result in cache if |download_to_string_cache| is used
    using DownloadErrorHandler = std::function<bool(std::string&)>;

    DownloadResult download_head_to_string(const std::string &url, std::string &result, bool use_browser_useragent = false, bool fail_on_error = true);
    // Returns the remote name from the content-disposition header or tries to extract the file name from url. Can return empty name
    DownloadResult url_get_remote_name(const std::string &url, std::string &result, bool use_browser_useragent);
    // Note: if |cloudflare_bypass| is set to true then tls is limited to version 1.1 and the user agent is changed
    DownloadResult download_to_string(const std::string &url, std::string &result, const std::vector<CommandArg> &additional_args, bool use_browser_useragent = false, bool fail_on_error = true, bool cloudflare_bypass = false, std::vector<std::string> *headers = nullptr, int download_limit = 1024 * 1024 * 100); // 100mb download limit
    // Note: This function saves the content to the file atomically
    DownloadResult download_to_string_cache(const std::string &url, std::string &result, const std::vector<CommandArg> &additional_args, bool use_browser_useragent = false, DownloadErrorHandler error_handler = nullptr, Path cache_path = "");
    // Note: This function saves the content to the file atomically.
    // Note: if |cloudflare_bypass| is set to true then tls is limited to version 1.1 and the user agent is changed.
    DownloadResult download_to_file(const std::string &url, const std::string &destination_filepath, const std::vector<CommandArg> &additional_args, bool use_browser_useragent = false, bool cloudflare_bypass = false);
    // Returns false if there was an error trying to create the download process
    bool download_async_gui(const std::string &url, const std::string &file_manager_start_dir, bool no_video);
    DownloadResult download_to_json(const std::string &url, rapidjson::Document &result, const std::vector<CommandArg> &additional_args, bool use_browser_useragent = false, bool fail_on_error = true);
}