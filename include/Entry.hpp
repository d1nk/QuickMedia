#pragma once

#include "Text.hpp"
#include "RoundedRectangle.hpp"
#include <mglpp/graphics/Text.hpp>
#include <functional>

namespace mgl {
    class Font;
    class Event;
    class Window;
    class Shader;
}

namespace QuickMedia {
    // Return true to clear the text
    using OnEntrySubmit = std::function<bool(std::string text)>;

    class Entry {
    public:
        Entry(const std::string &placeholder_text, mgl::Shader *rounded_rectangle_shader);
        void process_event(mgl::Window &window, mgl::Event &event);
        void draw(mgl::Window &window);

        void set_single_line(bool single_line);
        void set_editable(bool editable);
        void set_text(std::string text);
        void set_position(const mgl::vec2f &pos);
        void set_max_width(float width);
        void move_caret_to_end();
        void insert_text_at_caret_position(const std::string &str);

        void replace(size_t start_index, size_t length, const std::string &insert_str);
        int get_caret_index() const;

        bool is_editable() const;
        float get_height();
        const std::string& get_text() const;

        OnEntrySubmit on_submit_callback;
        bool draw_background;
    private:
        Text text;
        float width;
        RoundedRectangle background;
        mgl::Text placeholder;
        bool mouse_left_inside;
    };
}