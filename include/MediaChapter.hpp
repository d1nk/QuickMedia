#pragma once

#include <string>

namespace QuickMedia {
    struct MediaChapter {
        int start_seconds = 0;
        std::string title; // newlines will be replaced by spaces
    };
}