#pragma once

namespace QuickMedia {
    enum class PageType {
        EXIT,
        VIDEO_CONTENT,
        IMAGES,
        IMAGES_CONTINUOUS,
        IMAGE_BOARD_THREAD,
        CHAT_LOGIN,
        CHAT,
        FILE_MANAGER,
        CHAT_INVITE
    };
}