#pragma once

#include <string>

namespace QuickMedia {
    class Path {
    public:
        Path() = default;

        Path(const char *path) : data(path) {}
        Path(const std::string &path) : data(path) {}

        // TODO: Return a copy instead? makes it easier to use. Do the same for append
        Path& join(const Path &other) {
            data += "/";
            data += other.data;
            return *this;
        }

        Path& append(const std::string &str) {
            data += str;
            return *this;
        }

        // Includes extension
        const char* filename() const {
            size_t index = data.rfind('/');
            if(index == std::string::npos)
                return data.c_str();
            return data.c_str() + index + 1;
        }

        std::string filename_no_ext() const {
            const char *name = filename();
            const char *extension = ext();
            if(extension[0] == '\0')
                return name;
            else
                return data.substr(name - data.data(), extension - name);
        }

        // Returns extension with the dot. Returns empty string if no extension
        const char* ext() const {
            size_t slash_index = data.rfind('/');
            size_t index = data.rfind('.');
            if(index != std::string::npos && (slash_index == std::string::npos || index > slash_index))
                return data.c_str() + index;
            return "";
        }

        Path parent() {
            size_t slash_index = data.rfind('/');
            if(slash_index != std::string::npos && slash_index > 0)
                return Path(data.substr(0, slash_index));
            if(!data.empty() && data[0] == '/')
                return Path("/");
            else
                return Path("");
        }

        std::string data;
    };
}