#pragma once

namespace mgl {
    class Font;
    class Texture;
}

namespace QuickMedia {
    void set_resource_loader_root_path(const char *resource_root);
    const char* get_resource_loader_root_path();
    void set_use_system_fonts(bool use);
}

namespace QuickMedia::FontLoader {
    enum class FontType {
        LATIN,
        LATIN_BOLD,
        CJK,
        SYMBOLS
    };

    // Note: not thread-safe
    mgl::Font* get_font(FontType font_type, unsigned int character_size);
}

namespace QuickMedia::TextureLoader {
    // Note: not thread-safe. Filepath is relative to the resource loader root path
    mgl::Texture* get_texture(const char *filepath, bool pixel_coordinates = false);
}