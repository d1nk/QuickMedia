#pragma once

#include <mglpp/graphics/Color.hpp>
#include <mglpp/graphics/Vertex.hpp>
#include <mglpp/system/vec.hpp>

namespace mgl {
    class Window;
    class Shader;
}

namespace QuickMedia {
    class RoundedRectangle {
    public:
        RoundedRectangle(mgl::vec2f size, float radius, mgl::Color color, mgl::Shader *rounded_rectangle_shader);
        void set_position(mgl::vec2f pos);
        void set_size(mgl::vec2f size);
        void set_color(mgl::Color color);
        mgl::vec2f get_position() const;
        mgl::vec2f get_size() const;
        void draw(mgl::Window &target);
    private:
        float radius;
        mgl::vec2f pos;
        mgl::vec2f size;
        mgl::Vertex vertices[4];
        mgl::Shader *rounded_rectangle_shader;
    };
}