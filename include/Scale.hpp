#pragma once

#include <mglpp/system/vec.hpp>

namespace QuickMedia {
    template<typename T, typename A>
    static T wrap_to_size_x(const T &size, A clamp_size) {
        T new_size;
        if(size.x == 0) {
            new_size.x = 0;
            new_size.y = 0;
            return new_size;
        }
        float size_ratio = (float)size.y / (float)size.x;
        new_size.x = clamp_size;
        new_size.y = new_size.x * size_ratio;
        return new_size;
    }

    template<typename T, typename A>
    static T wrap_to_size_y(const T &size, A clamp_size) {
        T new_size;
        if(size.y == 0) {
            new_size.x = 0;
            new_size.y = 0;
            return new_size;
        }
        float size_ratio = (float)size.x / (float)size.y;
        new_size.y = clamp_size;
        new_size.x = new_size.y * size_ratio;
        return new_size;
    }

    template<typename T>
    static T wrap_to_size(const T &size, const T &clamp_size) {
        T new_size;
        new_size = wrap_to_size_x(size, clamp_size.x);
        if(new_size.y > clamp_size.y)
            new_size = wrap_to_size_y(size, clamp_size.y);
        return new_size;
    }

    template<typename T, typename A>
    static T clamp_to_size_x(const T &size, A clamp_size) {
        T new_size = size;
        if(size.x > clamp_size)
            new_size = wrap_to_size_x(new_size, clamp_size);
        return new_size;
    }

    template<typename T, typename A>
    static T clamp_to_size_y(const T &size, const A clamp_size) {
        T new_size = size;
        if(size.y > clamp_size)
            new_size = wrap_to_size_y(new_size, clamp_size);
        return new_size;
    }

    template<typename T>
    static T clamp_to_size(const T &size, const T &clamp_size) {
        T new_size = size;
        if(size.x > clamp_size.x || size.y > clamp_size.y)
            new_size = wrap_to_size(new_size, clamp_size);
        return new_size;
    }

    template<typename T>
    static mgl::vec2f get_ratio(const T &original_size, const T &new_size) {
        if(original_size.x == 0 || original_size.y == 0)
            return mgl::vec2f(0.0f, 0.0f);
        else
            return mgl::vec2f((float)new_size.x / (float)original_size.x, (float)new_size.y / (float)original_size.y);
    }
}