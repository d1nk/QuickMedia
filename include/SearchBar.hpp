#pragma once

#include "RoundedRectangle.hpp"
#include <mglpp/window/Event.hpp>
#include <mglpp/graphics/Text.hpp>
#include <mglpp/graphics/Rectangle.hpp>
#include <mglpp/graphics/Sprite.hpp>
#include <mglpp/system/Clock.hpp>
#include <functional>
#include <string>

namespace mgl {
    class Font;
    class Window;
    class Shader;
}

namespace QuickMedia {
    using TextUpdateCallback = std::function<void(const std::string &text)>;
    using TextSubmitCallback = std::function<void(const std::string &text)>;
    using TextBeginTypingCallback = std::function<void()>;

    class SearchBar {
    public:
        SearchBar(mgl::Texture *plugin_logo, mgl::Shader *rounded_rectangle_shader, const std::string &placeholder, bool input_masked = false);
        void draw(mgl::Window &window, mgl::vec2f size, bool draw_background);
        void on_event(mgl::Window &window, mgl::Event &event);
        void update();
        void onWindowResize(const mgl::vec2f &window_size);
        void clear();
        void set_text(const std::string &text, bool update_search = true);
        void append_text(const std::string &text_to_add);
        void set_position(mgl::vec2f pos);
        void set_editable(bool editable);

        bool is_editable() const;
        float getBottom() const;
        float getBottomWithoutShadow() const;

        std::string get_text() const;
        bool is_empty() const;

        TextUpdateCallback onTextUpdateCallback;
        TextSubmitCallback onTextSubmitCallback;
        TextBeginTypingCallback onTextBeginTypingCallback;
        int text_autosearch_delay_ms;
        bool caret_visible;

        float padding_top = 0.0f;
        float padding_bottom = 0.0f;
        float padding_x = 10.0f;
    private:
        void onTextEntered(const mgl::Event::TextEvent &text_event);
    private:
        mgl::Text text;
        RoundedRectangle background;
        mgl::Rectangle shade;
        mgl::Rectangle caret;
        mgl::Sprite plugin_logo_sprite;
        std::string placeholder_str;
        bool show_placeholder;
        bool updated_search;
        bool draw_logo;
        bool needs_update;
        bool input_masked;
        bool typing;
        bool backspace_pressed;
        bool mouse_left_inside;
        mgl::vec2f pos;
        mgl::Clock time_since_search_update;
        mgl::vec2f prev_size;
        bool editable = true;
    };
}