#pragma once

#include "Path.hpp"
#include <functional>
#include <rapidjson/fwd.h>

namespace Json {
    class Value;
}

namespace QuickMedia {
    enum class FileType {
        FILE_NOT_FOUND,
        REGULAR,
        DIRECTORY
    };

    enum FileSortDirection {
        ASC,
        DESC
    };

    // Return false to stop the iterator
    using FileIteratorCallback = std::function<bool(const Path &filepath, FileType file_type)>;

    Path get_home_dir();
    Path get_storage_dir();
    Path get_cache_dir();
    int get_cookies_filepath(Path &path, const std::string &plugin_name);
    int create_directory_recursive(const Path &path);
    FileType get_file_type(const Path &path);
    int file_get_content(const Path &path, std::string &result);
    int file_get_size(const Path &path, size_t *size);
    bool file_get_last_modified_time_seconds(const char *path, time_t *result);
    int file_overwrite(const Path &path, const std::string &data);
    int file_overwrite_atomic(const Path &path, const std::string &data);
    void for_files_in_dir(const Path &path, FileIteratorCallback callback);
    void for_files_in_dir_sort_last_modified(const Path &path, FileIteratorCallback callback, FileSortDirection sort_dir = FileSortDirection::ASC);
    void for_files_in_dir_sort_name(const Path &path, FileIteratorCallback callback, FileSortDirection sort_dir = FileSortDirection::ASC);

    bool read_file_as_json(const Path &filepath, Json::Value &result);
    bool save_json_to_file_atomic(const Path &path, const Json::Value &json);
    bool save_json_to_file_atomic(const Path &path, const rapidjson::Value &json);

    int rename_atomic(const char *oldpath, const char *newpath);

    bool is_program_executable_by_name(const char *name);

    std::string file_size_to_human_readable_string(size_t bytes);
}