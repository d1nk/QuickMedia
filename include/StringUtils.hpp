#pragma once

#include <string>
#include <functional>

namespace QuickMedia {
    // Return false to stop iterating
    using StringSplitCallback = std::function<bool(const char *str, size_t size)>;

    void string_split(const std::string &str, const std::string &delimiter, StringSplitCallback callback_func);
    void string_split(const std::string &str, char delimiter, StringSplitCallback callback_func);
    // Returns the number of replaced substrings
    size_t string_replace_all(std::string &str, char old_char, char new_char);
    // Returns the number of replaced substrings
    size_t string_replace_all(std::string &str, char old_char, const std::string &new_str);
    // Returns the number of replaced substrings
    size_t string_replace_all(std::string &str, const std::string &old_str, const std::string &new_str);
    std::string strip(const std::string &str);
    void strip(const char *str, size_t size, size_t *new_size);
    bool string_starts_with(const std::string &str, const char *sub);
    bool string_ends_with(const std::string &str, const std::string &ends_with_str);
    size_t str_find_case_insensitive(const std::string &str, size_t start_index, const char *substr, size_t substr_len);
    bool string_find_fuzzy_case_insensitive(const std::string &str, const std::string &substr);
    char to_upper(char c);
    bool strncase_equals(const char *str1, const char *str2, size_t length);
    bool strcase_equals(const char *str1, const char *str2);
    // Note: does not check for overflow
    bool to_num(const char *str, size_t size, int &num);
    // Note: does not check for overflow
    bool to_num_hex(const char *str, size_t size, int &num);
    std::string seconds_to_relative_time_str(time_t seconds);
    std::string seconds_to_duration(int seconds);
    std::string number_separate_thousand_commas(const std::string &number);
}