#pragma once

#include "RoundedRectangle.hpp"
#include <mglpp/graphics/Rectangle.hpp>
#include <mglpp/graphics/Text.hpp>
#include <mglpp/graphics/Sprite.hpp>
#include <vector>
#include <functional>

namespace mgl {
    class Event;
    class Window;
    class Shader;
}

namespace QuickMedia {
    class Body;

    class Tabs {
    public:
        Tabs(mgl::Shader *rounded_rectangle_shader);
        Tabs(mgl::Shader *rounded_rectangle_shader, mgl::Color shade_color);

        static float get_height();
        static float get_shade_height();

        // Returns the id (index) of the tab. The ids start from 0
        int add_tab(std::string title, Body *body);
        void on_event(mgl::Event &event);
        void draw(mgl::Window &window, mgl::vec2f pos, float width);

        void set_text(int index, const std::string &text);

        void set_selected(int index);
        int get_selected() const;

        std::function<void(int prev_tab, int new_tab)> on_change_tab = nullptr;
    private:
        void move_selected_tab(int new_tab);
        float tab_index_to_x_offset(int index);
    private:
        struct Tab {
            mgl::Text text;
            Body *body;
        };
        std::vector<Tab> tabs;
        RoundedRectangle background;
        mgl::Rectangle shade;
        int selected_tab = 0;
        float scroll = 0.0f;
        float width_per_tab = 0.0f;
        float tab_background_width = 0.0f;
        float container_width = 0.0f;
        int tab_offset = 0;
        mgl::Color shade_color;
        mgl::Sprite arrow_sprite;
    };
}