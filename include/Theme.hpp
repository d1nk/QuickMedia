#pragma once

#include <mglpp/graphics/Color.hpp>

namespace QuickMedia {
    struct Theme {
        Theme() = default;
        Theme(const Theme&) = delete;
        Theme&operator=(const Theme&) = delete;

        mgl::Color background_color = mgl::Color(18, 21, 26);
        mgl::Color text_color = mgl::Color(255, 255, 255);
        mgl::Color faded_text_color = mgl::Color(255, 255, 255, 179);
        mgl::Color shade_color = mgl::Color(28, 32, 39);
        mgl::Color selected_color = mgl::Color(55, 60, 68);
        mgl::Color card_item_background_color = mgl::Color(28, 32, 39);
        mgl::Color replies_text_color = mgl::Color(129, 162, 190);
        mgl::Color placeholder_text_color = mgl::Color(255, 255, 255, 100);
        mgl::Color image_loading_background_color = mgl::Color(52, 58, 70);
        mgl::Color attention_alert_text_color = mgl::Color(255, 100, 100);
        mgl::Color cancel_button_background_color = mgl::Color(41, 45, 50);
        mgl::Color confirm_button_background_color = mgl::Color(31, 117, 255);
        mgl::Color loading_bar_color = mgl::Color(31, 117, 255);
        mgl::Color embedded_item_border_color = mgl::Color(255, 255, 255);
        mgl::Color provisional_message_color = mgl::Color(255, 255, 255, 150);
        mgl::Color failed_text_color = mgl::Color(255, 0, 0);
        mgl::Color timestamp_text_color = mgl::Color(185, 190, 198, 100);
        mgl::Color new_items_alert_color = mgl::Color(128, 50, 50);
        mgl::Color arrow_color = mgl::Color(255, 255, 255, 175);
        mgl::Color url_text_color = mgl::Color(35, 140, 245);
        mgl::Color loading_page_color = mgl::Color(175, 180, 188);
        mgl::Color more_items_color = mgl::Color(150, 175, 255, 100);
        bool drop_shadow = false;
    };

    const Theme& get_theme();
}
