#pragma once

#include <time.h>
#include <string>
#include <mglpp/system/vec.hpp>

namespace QuickMedia {
    void show_virtual_keyboard();
    void hide_virtual_keyboard();
    bool is_touch_enabled();
    bool is_running_wayland();
    time_t iso_utc_to_unix_time(const char *time_str);
    std::string unix_time_to_local_time_str(time_t unix_time);
    int64_t get_boottime_milliseconds();

    mgl::vec2f vec2f_floor(float x, float y);
}