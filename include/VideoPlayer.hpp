#pragma once

#include "MediaChapter.hpp"
#include <mglpp/window/Window.hpp>
#include <mglpp/system/Clock.hpp>
#include <functional>
#include <json/value.h>
#include <X11/Xlib.h>

namespace QuickMedia {
    using EventCallbackFunc = std::function<void(const char *event_name)>;
    using VideoPlayerWindowCreateCallback = std::function<void(mgl::WindowHandle window)>;

    // Currently this video player launches mpv and embeds it into the QuickMedia window
    class VideoPlayer {
    public:
        enum class Error {
            OK,
            FAIL_TO_GENERATE_IPC_FILENAME,
            FAIL_TO_LAUNCH_PROCESS,
            FAIL_TO_CREATE_SOCKET,
            FAIL_TO_CONNECT_TIMEOUT,
            FAIL_NOT_CONNECTED,
            FAIL_TO_SEND,
            FAIL_TO_FIND_WINDOW,
            FAIL_TO_FIND_WINDOW_TIMEOUT,
            UNEXPECTED_WINDOW_ERROR,
            FAIL_TO_READ,
            READ_TIMEOUT,
            READ_RESPONSE_ERROR,
            READ_INCORRECT_TYPE,
            INIT_FAILED,
            EXITED
        };

        struct StartupArgs {
            std::string path;
            std::string audio_path;
            mgl::WindowHandle parent_window;
            bool no_video = false;
            bool use_system_mpv_config = false;
            bool use_system_input_config = false; // |use_system_mpv_config| has to be true if this is set to true
            bool keep_open = false;
            bool resume = false;
            std::string resource_root;
            int monitor_height = 1080;
            bool use_youtube_dl = false;
            std::string title;
            std::string start_time;
            std::vector<MediaChapter> chapters;
            std::string plugin_name;
        };

        // Important: do not call |get_time_in_file| or |add_subtitle| from the |event_callback| callback
        VideoPlayer(StartupArgs startup_args, EventCallbackFunc event_callback, VideoPlayerWindowCreateCallback window_create_callback);
        ~VideoPlayer();
        VideoPlayer(const VideoPlayer&) = delete;
        VideoPlayer& operator=(const VideoPlayer&) = delete;

        // |audio_path| is only set when video and audio are separate files/urls.
        Error load_video();
        // Should be called every update frame
        Error update();

        // Returns time in seconds
        Error get_time_in_file(double *result);
        Error add_subtitle(const std::string &url, const std::string &title, const std::string &lang);

        int exit_status;
    private:
        uint32_t get_next_request_id();
        Error send_command(Json::Value &json_root, Json::Value *result, Json::ValueType result_type);
        Error launch_video_process();
        VideoPlayer::Error read_ipc_func();
    private:
        StartupArgs startup_args;

        pid_t video_process_id;
        mgl::Clock retry_timer;
        int connect_tries;
        int find_window_tries;
        int ipc_socket = -1;
        EventCallbackFunc event_callback;
        VideoPlayerWindowCreateCallback window_create_callback;
        mgl::WindowHandle window_handle;
        Display *display;
        unsigned int request_id_counter;
        unsigned int expected_request_id;
        Json::Value request_response_data;

        enum ResponseDataStatus {
            NONE,
            OK,
            ERROR
        };
        ResponseDataStatus response_data_status;
        char tmp_chapters_filepath[27];
    };
}
