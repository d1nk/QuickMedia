#pragma once

#include "../RoundedRectangle.hpp"
#include <string>
#include <mglpp/graphics/Text.hpp>

namespace mgl {
    class Event;
    class Font;
    class Window;
    class Shader;
}

namespace QuickMedia {
    enum ButtonEvent {
        BUTTON_EVENT_NONE    = 0,
        BUTTON_EVENT_CLICKED = 1
    };

    class Button {
    public:
        Button(const std::string &label, mgl::Font *font, float width, mgl::Shader *rounded_rectangle_shader, float scale = 1.0f);

        ButtonEvent on_event(mgl::Event &event);
        void draw(mgl::Window &target);

        void set_background_color(mgl::Color color);
        void set_position(mgl::vec2f pos);

        mgl::vec2f get_position() const;
        float get_width() const;
        float get_height();
    private:
        mgl::Text label;
        RoundedRectangle background;
        mgl::Color background_color;
        float scale;
        bool clicked_inside = false;
    };
}