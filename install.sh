#!/bin/sh -e

script_dir=$(dirname "$0")
cd "$script_dir"

[ $(id -u) -ne 0 ] && echo "You need root privileges to run the install script" && exit 1

sibs build --release video_player
sibs build --release

install -Dm755 "video_player/sibs-build/$(sibs platform)/release/quickmedia-video-player" "/usr/bin/quickmedia-video-player"
install -Dm755 "sibs-build/$(sibs platform)/release/quickmedia" "/usr/bin/quickmedia"
ln -sf "/usr/bin/quickmedia" "/usr/bin/qm"
install -Dm644 boards.json "/usr/share/quickmedia/boards.json"
install -Dm644 input.conf "/usr/share/quickmedia/input.conf"

for file in images/* icons/* shaders/* themes/*; do
    install -Dm644 "$file" "/usr/share/quickmedia/$file"
done

for file in launcher/*; do
    filename=$(basename "$file")
    install -Dm644 "$file" "/usr/share/applications/$filename"
done

echo "Successfully installed QuickMedia"
