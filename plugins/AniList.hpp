#pragma once

#include "Page.hpp"

namespace QuickMedia {
    enum class AniListMediaType {
        ANIME,
        MANGA
    };

    class AniListSearchPage : public Page {
    public:
        AniListSearchPage(Program *program, AniListMediaType media_type) : Page(program), media_type(media_type) {}
        const char* get_title() const override { return media_type == AniListMediaType::ANIME ? "Search for anime" : "Search for manga"; }
        bool search_is_filter() override { return false; }
        bool submit_is_async() const override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
    private:
        SearchResult search_page(const std::string &str, int page, BodyItems &result_items);
    private:
        AniListMediaType media_type;
    };

    class AniListRelatedPage : public LazyFetchPage {
    public:
        AniListRelatedPage(Program *program, std::string id) : LazyFetchPage(program), id(std::move(id)) {}
        const char* get_title() const override { return "Related"; }
        bool submit_is_async() const override { return false; }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
    private:
        std::string id;
    };

    class AniListDetailsPage : public LazyFetchPage {
    public:
        AniListDetailsPage(Program *program, std::string id) : LazyFetchPage(program), id(std::move(id)) {}
        const char* get_title() const override { return "Details"; }
        bool submit_is_async() const override { return false; }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
    private:
        std::string id;
    };

    class AniListRecommendationsPage : public LazyFetchPage {
    public:
        AniListRecommendationsPage(Program *program, std::string id) : LazyFetchPage(program), id(std::move(id)) {}
        const char* get_title() const override { return "Recommendations"; }
        bool submit_is_async() const override { return false; }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
    private:
        std::string id;
    };
}