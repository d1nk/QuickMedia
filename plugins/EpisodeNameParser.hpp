#pragma once

#include <string_view>
#include <optional>

namespace QuickMedia {
    struct EpisodeNameParts {
        std::string_view group;     // optional
        std::string_view anime;     // required
        std::string_view season;    // optional
        std::string_view episode;   // required
    };

    std::optional<EpisodeNameParts> episode_name_extract_parts(std::string_view episode_name);
}