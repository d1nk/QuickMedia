#pragma once

#include "Page.hpp"
#include <filesystem>

namespace QuickMedia {
    class FileManagerPage;

    enum FileManagerMimeType {
        FILE_MANAGER_MIME_TYPE_IMAGE = (1 << 0),
        FILE_MANAGER_MIME_TYPE_VIDEO = (1 << 1),
        FILE_MANAGER_MIME_TYPE_OTHER = (1 << 2)
    };

    static const FileManagerMimeType FILE_MANAGER_MIME_TYPE_ALL = (FileManagerMimeType)(FILE_MANAGER_MIME_TYPE_IMAGE|FILE_MANAGER_MIME_TYPE_VIDEO|FILE_MANAGER_MIME_TYPE_OTHER);
    // Return the tags to go to after selecting a file, or return an empty array to exit the program
    using FileSelectionHandler = std::function<std::vector<Tab>(FileManagerPage*, const std::filesystem::path&)>;

    class FileManagerPage : public Page {
    public:
        FileManagerPage(Program *program, FileManagerMimeType mime_type = FILE_MANAGER_MIME_TYPE_ALL, FileSelectionHandler selection_handler = nullptr) :
            Page(program), current_dir("/"), mime_type(mime_type), selection_handler(selection_handler) {}
        const char* get_title() const override { return current_dir.c_str(); }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        bool is_single_page() const override { return true; }

        bool set_current_directory(const std::string &path);
        const std::filesystem::path& get_current_directory() const { return current_dir; }
        PluginResult get_files_in_directory(BodyItems &result_items);

        // |fallback| is used if no directory has been accessed
        static std::filesystem::path& get_last_accessed_directory(std::filesystem::path &fallback);
    private:
        std::filesystem::path current_dir;
        FileManagerMimeType mime_type;
        FileSelectionHandler selection_handler;
        bool allow_empty_match_submit;
    };
}