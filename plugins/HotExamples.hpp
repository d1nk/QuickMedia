#pragma once

#include "Page.hpp"

namespace QuickMedia {
    void hot_examples_front_page_fill(BodyItems &body_items);

    class HotExamplesLanguageSelectPage : public Page {
    public:
        HotExamplesLanguageSelectPage(Program *program) : Page(program) {}
        const char* get_title() const override { return "Select language"; }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        bool submit_is_async() const override { return false; }
    };

    class HotExamplesSearchPage : public Page {
    public:
        HotExamplesSearchPage(Program *program, const std::string &language) : Page(program), language(language) {}
        const char* get_title() const override { return "Select result"; }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
    private:
        std::string language;
    };

    class HotExamplesCodeExamplesPage : public Page {
    public:
        HotExamplesCodeExamplesPage(Program *program, std::string title) : Page(program), title(std::move(title)) {}
        const char* get_title() const override { return title.c_str(); }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab>&) override { (void)args; return PluginResult::OK; }
        bool submit_is_async() const override { return false; }
    private:
        std::string title;
    };
}