#pragma once

#include "Page.hpp"

namespace QuickMedia {
    enum class PostResult {
        OK,
        TRY_AGAIN,
        INVALID_CAPTCHA,
        BANNED,
        //FILE_TOO_LARGE,
        NO_SUCH_FILE,
        FILE_TYPE_NOT_ALLOWED,
        UPLOAD_FAILED,
        ERR
    };

    // All fields are optional
    struct ImageBoardCaptchaChallenge {
        std::string challenge_id;
        std::string img_data;
        std::string bg_data;
        int ttl = 0;
    };

    class ImageBoardThreadPage : public VideoPage {
    public:
        ImageBoardThreadPage(Program *program, std::string board_id, std::string thread_id) : VideoPage(program, ""), board_id(std::move(board_id)), thread_id(std::move(thread_id)) {}
        const char* get_title() const override { return ""; }
        PageTypez get_type() const override { return PageTypez::IMAGE_BOARD_THREAD; }

        void copy_to_clipboard(const BodyItem *body_item) override;

        bool autoplay_next_item() override { return true; }
        virtual PluginResult login(const std::string &token, const std::string &pin, std::string &response_msg);
        // If |filepath| is empty then no file is uploaded
        virtual PostResult post_comment(const std::string &captcha_id, const std::string &captcha_solution, const std::string &comment, const std::string &filepath = "") = 0;
        virtual const std::string& get_pass_id();

        virtual PluginResult request_captcha_challenge(ImageBoardCaptchaChallenge &challenge_response) = 0;

        const std::string board_id;
        const std::string thread_id;
    };
}