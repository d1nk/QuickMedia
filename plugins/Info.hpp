#pragma once

#include "Page.hpp"

namespace QuickMedia {
    class InfoPage : public Page {
    public:
        InfoPage(Program *program) : Page(program) {}
        const char* get_title() const override { return "Info"; }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        bool submit_is_async() const override { return false; }

        void copy_to_clipboard(const BodyItem *body_item) override;

        static std::shared_ptr<BodyItem> add_url(const std::string &url);
        static std::shared_ptr<BodyItem> add_reverse_image_search(const std::string &image_url);
        static std::shared_ptr<BodyItem> add_google_search(const std::string &search_term);
    };
}