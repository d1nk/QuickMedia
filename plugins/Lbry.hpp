#pragma once

#include "Page.hpp"

namespace QuickMedia {
    class LbrySearchPage : public Page {
    public:
        LbrySearchPage(Program *program, std::string channel_id = "") : Page(program), channel_id(std::move(channel_id)) {}
        const char* get_title() const override { return "Search"; }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        bool submit_is_async() const override { return false; }
    private:
        std::string channel_id;
    };

    class LbryChannelPage : public LazyFetchPage {
    public:
        LbryChannelPage(Program *program, std::string title, std::string channel_id) : 
            LazyFetchPage(program), search_page(program, channel_id), title(std::move(title)), channel_id(std::move(channel_id)) {}
        const char* get_title() const override { return title.c_str(); }
        bool submit_is_async() const override { return false; }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
    private:
        LbrySearchPage search_page;
        std::string title;
        std::string channel_id;
    };

    class LbryVideoPage : public VideoPage {
    public:
        LbryVideoPage(Program *program, std::string title, std::string url) : VideoPage(program, std::move(url)), title(std::move(title)) {}
        const char* get_title() const override { return ""; }
        //BodyItems get_related_media(const std::string &url) override;
        std::string get_download_url(int max_height) override;
        std::string get_video_url(int max_height, bool &has_embedded_audio, std::string &ext) override;
        std::string get_audio_url(std::string &ext) override;
        PluginResult load(std::string &title, std::string &channel_url, std::vector<MediaChapter> &chapters, std::string &err_str) override;
    private:
        std::string title;
        std::string streaming_url;
    };
}