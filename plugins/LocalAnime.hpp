#pragma once

#include "Page.hpp"
#include "WatchProgress.hpp"
#include <vector>
#include <variant>

namespace QuickMedia {
    struct LocalAnime;
    struct LocalAnimeSeason;
    struct LocalAnimeEpisode;
    using LocalAnimeItem = std::variant<LocalAnime, LocalAnimeSeason, LocalAnimeEpisode>;

    struct LocalAnimeEpisode {
        Path path;
        time_t modified_time_seconds;
    };

    struct LocalAnimeSeason {
        std::string name;
        std::vector<LocalAnimeItem> episodes;
        time_t modified_time_seconds;
    };

    struct LocalAnime {
        std::string name;
        std::vector<LocalAnimeItem> items;
        time_t modified_time_seconds;
    };

    std::vector<LocalAnimeItem> get_anime_in_directory(const Path &directory);

    class LocalAnimeSearchPage : public LazyFetchPage {
    public:
        LocalAnimeSearchPage(Program *program, std::vector<LocalAnimeItem> anime_items)
            : LazyFetchPage(program), anime_items(std::move(anime_items)) {}
        const char* get_title() const override { return "Search"; }
        bool search_is_filter() override { return true; }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
        void toggle_read(BodyItem *selected_item) override;
    private:
        std::vector<LocalAnimeItem> anime_items;
    };

    class LocalAnimeVideoPage : public VideoPage {
    public:
        LocalAnimeVideoPage(Program *program, std::string filepath, WatchProgress *watch_progress)
            : VideoPage(program, std::move(filepath)), watch_progress(watch_progress) {}
        const char* get_title() const override { return ""; }
        std::string get_video_url(int max_height, bool &has_embedded_audio, std::string &ext) override;
        std::string get_url_timestamp() override;
        bool is_local() const override { return true; }
        void set_watch_progress(int64_t time_pos_sec) override;
    private:
        WatchProgress *watch_progress;
    };
}