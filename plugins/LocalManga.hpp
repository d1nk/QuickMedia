#pragma once

#include "Manga.hpp"
#include <unordered_set>

namespace QuickMedia {
    struct LocalMangaPage {
        Path path;
        int number;
    };

    struct LocalMangaChapter {
        std::string name;
        std::vector<LocalMangaPage> pages;
    };

    struct LocalManga {
        std::string name;
        std::vector<LocalMangaChapter> chapters;
        time_t modified_time_seconds;
    };

    class LocalMangaSearchPage : public LazyFetchPage {
    public:
        LocalMangaSearchPage(Program *program, bool standalone) : LazyFetchPage(program), standalone(standalone) {}
        const char* get_title() const override { return "Search"; }
        bool search_is_filter() override { return standalone; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
        const char* get_bookmark_name() const override { return "local-manga"; }
        bool reload_on_page_change() override { return true; }
        bool reseek_to_body_item_by_url() override { return true; }
        std::shared_ptr<BodyItem> get_bookmark_body_item(BodyItem *selected_item) override;
        void toggle_read(BodyItem *selected_item) override;
    private:
        std::vector<LocalManga> manga_list;
        std::unordered_set<std::string> finished_reading_manga;
        bool standalone;
    };

    class LocalMangaChaptersPage : public MangaChaptersPage {
    public:
        LocalMangaChaptersPage(Program *program, std::string manga_name, std::string manga_url, const std::string &thumbnail_url) : MangaChaptersPage(program, std::move(manga_name), std::move(manga_url), thumbnail_url) {}
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        const char* get_bookmark_name() const override { return "local-manga"; }
    protected:
        bool extract_id_from_url(const std::string &url, std::string &manga_id) const override;
        const char* get_service_name() const override { return "local-manga"; }
    };

    class LocalMangaImagesPage : public MangaImagesPage {
    public:
        LocalMangaImagesPage(Program *program, std::string manga_name, std::string chapter_name, std::string url, std::string thumbnail_url) :
            MangaImagesPage(program, std::move(manga_name), std::move(chapter_name), std::move(url), std::move(thumbnail_url)) {}
        ImageResult update_image_urls(int &num_images) override;
        ImageResult for_each_page_in_chapter(PageCallback callback) override;
        const char* get_service_name() const override { return "local-manga"; }
        const char* get_website_url() const override { return "localhost"; }
        bool is_local() override { return true; }
    };
}