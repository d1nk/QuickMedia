#pragma once

#include "Page.hpp"
#include <functional>

namespace QuickMedia {
    // Return false to stop iteration
    using PageCallback = std::function<bool(const std::string &url)>;

    enum class CreatorType {
        AUTHOR,
        ARTIST
    };

    struct Creator {
        std::string name;
        std::string url;
        CreatorType creator_type;
    };

    class MangaImagesPage : public Page {
    public:
        MangaImagesPage(Program *program, std::string manga_name, std::string chapter_name, std::string url, std::string thumbnail_url) :
            Page(program), manga_name(std::move(manga_name)), thumbnail_url(std::move(thumbnail_url)), chapter_name(std::move(chapter_name)), url(std::move(url)), chapter_num_pages(-1) {}
        virtual ~MangaImagesPage() = default;
        const char* get_title() const override { return chapter_name.c_str(); }
        PageTypez get_type() const override { return PageTypez::MANGA_IMAGES; }

        virtual ImageResult update_image_urls(int &num_images) = 0;
        virtual ImageResult for_each_page_in_chapter(PageCallback callback) = 0;

        virtual void change_chapter(std::string new_chapter_name, std::string new_url) {
            chapter_name = std::move(new_chapter_name);
            if(url != new_url) {
                url = std::move(new_url);
                chapter_image_urls.clear();
                chapter_num_pages = -1;
            }
        }

        const std::string& get_chapter_name() const { return chapter_name; }
        const std::string& get_url() const { return url; }

        // TODO: Remove and use plugin name instead
        virtual const char* get_service_name() const = 0;

        virtual const char* get_website_url() const = 0;

        virtual bool is_local() { return false; }

        const std::string manga_name;
        const std::string thumbnail_url;
    protected:
        std::string chapter_name;
        std::string url;
        std::vector<std::string> chapter_image_urls;
        int chapter_num_pages;
    };

    class MangaChaptersPage : public Page, public TrackablePage {
    public:
        MangaChaptersPage(Program *program, std::string manga_name, std::string manga_url, const std::string &thumbnail_url) :
            Page(program), TrackablePage(std::move(manga_name), std::move(manga_url)), thumbnail_url(thumbnail_url) {}
        const char* get_title() const override { return content_title.c_str(); }
        TrackResult track(const std::string &str) override;
        void on_navigate_to_page(Body *body) override;
        bool is_trackable() const override { return true; }
        std::shared_ptr<BodyItem> get_bookmark_body_item(BodyItem *selected_item) override;
    protected:
        virtual bool extract_id_from_url(const std::string &url, std::string &manga_id) const = 0;
        virtual const char* get_service_name() const = 0;
        std::string thumbnail_url;
    };
}