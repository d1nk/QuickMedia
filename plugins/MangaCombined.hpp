#pragma once

#include "Manga.hpp"
#include <vector>
#include <map>
#include "../include/AsyncTask.hpp"

namespace QuickMedia {
    struct MangaPlugin {
        std::unique_ptr<Page> page;
        std::string title;
        std::string service_name;
        bool local_manga = false;
    };

    using MangaCombinedSearchThread = std::pair<MangaPlugin*, AsyncTask<BodyItems>>;

    class MangaCombinedSearchPage : public LazyFetchPage {
    public:
        MangaCombinedSearchPage(Program *program, std::vector<MangaPlugin> search_pages);
        const char* get_title() const override { return "Search"; }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
        bool lazy_fetch_is_loader() override { return true; }
        void cancel_operation() override;
    private:
        std::vector<MangaPlugin> search_pages;
        std::vector<MangaCombinedSearchThread> search_threads; // TODO: Use async task pool
    };
}