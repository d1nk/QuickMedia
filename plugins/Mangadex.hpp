#pragma once

#include "Manga.hpp"
#include <functional>
#include <optional>

namespace QuickMedia {
    using ChapterImageUrls = std::unordered_map<std::string, std::vector<std::string>>;

    class MangadexSearchPage : public Page {
    public:
        MangadexSearchPage(Program *program) : Page(program) {}
        const char* get_title() const override { return "Search"; }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        const char* get_bookmark_name() const override { return "mangadex"; }
    private:
        bool get_rememberme_token(std::string &rememberme_token);
        std::optional<std::string> rememberme_token;
    };

    class MangadexChaptersPage : public MangaChaptersPage {
    public:
        MangadexChaptersPage(Program *program, MangadexSearchPage *search_page, std::string manga_name, std::string manga_url, const std::string &thumbnail_url) :
            MangaChaptersPage(program, std::move(manga_name), std::move(manga_url), thumbnail_url), search_page(search_page) {}
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        const char* get_bookmark_name() const override { return "mangadex"; }
    protected:
        bool extract_id_from_url(const std::string &url, std::string &manga_id) const override;
        const char* get_service_name() const override { return "mangadex"; }
    private:
        MangadexSearchPage *search_page;
    };

    class MangadexCreatorPage : public LazyFetchPage {
    public:
        MangadexCreatorPage(Program *program, MangadexSearchPage *search_page, Creator creator) : LazyFetchPage(program), search_page(search_page), creator(std::move(creator)) {}
        const char* get_title() const override { return creator.name.c_str(); }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
    private:
        MangadexSearchPage *search_page;
        Creator creator;
    };

    class MangadexImagesPage : public MangaImagesPage {
    public:
        MangadexImagesPage(Program *program, MangadexSearchPage *search_page, std::string manga_name, std::string chapter_id, std::string chapter_name, std::string thumbnail_url) :
            MangaImagesPage(program, std::move(manga_name), std::move(chapter_name), std::move(chapter_id), std::move(thumbnail_url)), search_page(search_page) {}
        ImageResult update_image_urls(int &num_images) override;
        ImageResult for_each_page_in_chapter(PageCallback callback) override;
        const char* get_service_name() const override { return "mangadex"; }
        const char* get_website_url() const override { return "https://mangadex.org/"; }
    private:
        MangadexSearchPage *search_page;
    };
}