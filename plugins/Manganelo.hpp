#pragma once

#include "Manga.hpp"

namespace QuickMedia {
    class ManganeloSearchPage : public Page {
    public:
        ManganeloSearchPage(Program *program) : Page(program) {}
        const char* get_title() const override { return "Search"; }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        const char* get_bookmark_name() const override { return "manganelo"; }
    };

    class ManganeloChaptersPage : public MangaChaptersPage {
    public:
        ManganeloChaptersPage(Program *program, std::string manga_name, std::string manga_url, const std::string &thumbnail_url) : MangaChaptersPage(program, std::move(manga_name), std::move(manga_url), thumbnail_url) {}
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        const char* get_bookmark_name() const override { return "manganelo"; }
    protected:
        bool extract_id_from_url(const std::string &url, std::string &manga_id) const override;
        const char* get_service_name() const override { return "manganelo"; }
    };

    class ManganeloCreatorPage : public LazyFetchPage {
    public:
        ManganeloCreatorPage(Program *program, Creator creator) : LazyFetchPage(program), creator(std::move(creator)) {}
        const char* get_title() const override { return creator.name.c_str(); }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
    private:
        Creator creator;
    };

    class ManganeloImagesPage : public MangaImagesPage {
    public:
        ManganeloImagesPage(Program *program, std::string manga_name, std::string chapter_name, std::string url, std::string thumbnail_url) :
            MangaImagesPage(program, std::move(manga_name), std::move(chapter_name), std::move(url), std::move(thumbnail_url)) {}
        ImageResult update_image_urls(int &num_images) override;
        ImageResult for_each_page_in_chapter(PageCallback callback) override;
        const char* get_service_name() const override { return "manganelo"; }
        const char* get_website_url() const override { return "https://manganelo.com/"; }
    };
}