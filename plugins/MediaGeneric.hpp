#pragma once

#include "Page.hpp"
#include <functional>

namespace QuickMedia {
    struct MediaSearchQuery {
        const char *search_template = nullptr;
        int page_start = 0;
    };

    // If |url_contains| is null, then any matching query is added. If |title_field| is "text", then the inner text is used.
    // If |url_field| is null, then the current page is used instead.
    struct MediaTextQuery {
        const char *html_query = nullptr;
        const char *title_field = nullptr;
        const char *url_field = nullptr;
        const char *url_contains = nullptr;
    };

    struct MediaThumbnailQuery {
        const char *html_query = nullptr;
        const char *field_name = nullptr;
        const char *field_contains = nullptr;
    };

    struct MediaRelatedItem {
        std::string title;
        std::string url;
        std::string thumbnail_url;
    };

    using MediaRelatedCustomHandler = std::function<std::vector<MediaRelatedItem>(const std::string &html_source)>;

    class MediaGenericSearchPage : public Page {
    public:
        MediaGenericSearchPage(Program *program, const char *website_url, mgl::vec2i thumbnail_max_size, bool cloudflare_bypass, std::vector<CommandArg> extra_commands = {});
        const char* get_title() const override { return "Search"; }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;

        PluginResult get_related_media(const std::string &url, BodyItems &result_items);

        // Add a %s where the search query should be inserted into |search_template| and add a %p where the page number should be inserted, for example:
        // example.com/search?q=%s&page=%p
        // This is required.
        MediaGenericSearchPage& search_handler(const char *search_template, int page_start);
        // This is required.
        MediaGenericSearchPage& text_handler(std::vector<MediaTextQuery> queries);
        // This is optional.
        MediaGenericSearchPage& thumbnail_handler(std::vector<MediaThumbnailQuery> queries);

        // This is optional.
        MediaGenericSearchPage& related_media_text_handler(std::vector<MediaTextQuery> queries);
        // This is optional.
        MediaGenericSearchPage& related_media_thumbnail_handler(std::vector<MediaThumbnailQuery> queries);
        // This is optional.
        MediaGenericSearchPage& related_media_custom_handler(MediaRelatedCustomHandler handler);
    private:
        std::string website_url;
        mgl::vec2i thumbnail_max_size;
        MediaSearchQuery search_query;
        std::vector<MediaTextQuery> text_queries;
        std::vector<MediaThumbnailQuery> thumbnail_queries;
        std::vector<MediaTextQuery> related_media_text_queries;
        std::vector<MediaThumbnailQuery> related_media_thumbnail_queries;
        MediaRelatedCustomHandler related_custom_handler = nullptr;
        bool cloudflare_bypass;
        std::vector<CommandArg> extra_commands;
    };

    class MediaGenericRelatedPage : public RelatedVideosPage {
    public:
        MediaGenericRelatedPage(Program *program, MediaGenericSearchPage *search_page) : RelatedVideosPage(program), search_page(search_page) {}
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
    private:
        MediaGenericSearchPage *search_page;
    };

    class MediaGenericVideoPage : public VideoPage {
    public:
        MediaGenericVideoPage(Program *program, MediaGenericSearchPage *search_page, const std::string &url) : VideoPage(program, url), search_page(search_page) {}
        const char* get_title() const override { return ""; }
        BodyItems get_related_media(const std::string &url) override;
        PluginResult get_related_pages(const BodyItems &related_videos, const std::string &channel_url, std::vector<Tab> &result_tabs) override;
    private:
        MediaGenericSearchPage *search_page;
    };
}