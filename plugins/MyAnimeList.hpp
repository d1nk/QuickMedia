#pragma once

#include "Page.hpp"

namespace QuickMedia {
    class MyAnimeListSearchPage : public Page {
    public:
        MyAnimeListSearchPage(Program *program) : Page(program) {}
        const char* get_title() const override { return "Search for anime or manga"; }
        bool search_is_filter() override { return false; }
        bool submit_is_async() const override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
    };

    class MyAnimeListDetailsPage : public LazyFetchPage {
    public:
        MyAnimeListDetailsPage(Program *program, std::string url) : LazyFetchPage(program), url(std::move(url)) {}
        const char* get_title() const override { return "Details"; }
        bool submit_is_async() const override { return false; }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
    private:
        std::string url;
    };

    class MyAnimeListRecommendationsPage : public LazyFetchPage {
    public:
        MyAnimeListRecommendationsPage(Program *program, std::string url) : LazyFetchPage(program), url(std::move(url)) {}
        const char* get_title() const override { return "Recommendations"; }
        bool submit_is_async() const override { return false; }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
    private:
        std::string url;
    };
}