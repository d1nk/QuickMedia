#pragma once

#include "Page.hpp"

namespace QuickMedia {
    void get_nyaa_si_categories(BodyItems &result_items);
    void get_sukebei_categories(BodyItems &result_items);

    class NyaaSiCategoryPage : public Page {
    public:
        NyaaSiCategoryPage(Program *program, bool is_sukebei) : Page(program), is_sukebei(is_sukebei) {}
        const char* get_title() const override { return is_sukebei ? "Select sukebei category" : "Select nyaa.si category"; }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;

        const bool is_sukebei;
    };

    enum class NyaaSiSortType {
        SIZE_DESC,
        UPLOAD_DATE_DESC,
        SEEDERS_DESC,
        LEECHERS_DESC,
        DOWNLOADS_DESC,

        SIZE_ASC,
        UPLOAD_DATE_ASC,
        SEEDERS_ASC,
        LEECHERS_ASC,
        DOWNLOADS_ASC
    };

    class NyaaSiSearchPage : public Page {
    public:
        NyaaSiSearchPage(Program *program, std::string category_name, std::string category_id, std::string domain);
        const char* get_title() const override { return title.c_str(); }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;

        void set_sort_type(NyaaSiSortType sort_type);

        const std::string category_name;
        const std::string category_id;
        const std::string domain;
    private:
        NyaaSiSortType sort_type = NyaaSiSortType::UPLOAD_DATE_DESC;
        std::string title;
    };

    class NyaaSiSortOrderPage : public Page {
    public:
        NyaaSiSortOrderPage(Program *program, Body *body, NyaaSiSearchPage *search_page) : Page(program), body(body), search_page(search_page) {}
        const char* get_title() const override { return "Sort order"; }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        bool submit_is_async() const override { return false; }
    private:
        Body *body;
        NyaaSiSearchPage *search_page;
    };

    class NyaaSiTorrentPage : public Page {
    public:
        NyaaSiTorrentPage(Program *program) : Page(program) {}
        const char* get_title() const override { return "Torrent"; }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        bool submit_is_async() const override { return false; }
    };
}