#pragma once

#include "Page.hpp"

namespace QuickMedia {
    class PeertubeInstanceSelectionPage : public LazyFetchPage {
    public:
        PeertubeInstanceSelectionPage(Program *program) : LazyFetchPage(program) {}
        const char* get_title() const override { return "Select instance"; }
        bool allow_submit_no_selection() const override { return true; }
        bool submit_is_async() const override { return false; }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
    };

    class PeertubeSearchPage : public LazyFetchPage {
    public:
        enum class SearchType {
            VIDEO_CHANNELS,
            VIDEO_PLAYLISTS,
            VIDEOS
        };

        PeertubeSearchPage(Program *program, const std::string &server);
        const char* get_title() const override { return "Search"; }
        bool search_is_filter() override { return false; }
        bool submit_is_async() const override { return false; }
        // Fetches local videos if |str| is empty
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        // Fetches all local videos
        PluginResult lazy_fetch(BodyItems &result_items) override;
    private:
        PluginResult get_local_videos(int page, BodyItems &result_items);
        PluginResult get_page_by_type(SearchType search_type, const std::string &str, int page, int count, BodyItems &result_items);
    private:
        std::string server;
    };

    class PeertubeChannelPage : public LazyFetchPage {
    public:
        PeertubeChannelPage(Program *program, const std::string &server, std::string display_name, std::string name) : 
            LazyFetchPage(program), server(server), name(std::move(name)), title(std::move(display_name)) {}
        const char* get_title() const override { return title.c_str(); }
        bool submit_is_async() const override { return false; }
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
    private:
        std::string server;
        std::string name;
        std::string title;
    };

    class PeertubePlaylistPage : public LazyFetchPage {
    public:
        PeertubePlaylistPage(Program *program, const std::string &server, std::string display_name, std::string uuid) : 
            LazyFetchPage(program), server(server), uuid(std::move(uuid)), title(std::move(display_name)) {}
        const char* get_title() const override { return title.c_str(); }
        bool submit_is_async() const override { return false; }
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
    private:
        std::string server;
        std::string uuid;
        std::string title;
    };

    class PeertubeVideoPage : public VideoPage {
    public:
        struct VideoSource {
            std::string url;
            int resolution; // 720p = 720. 0 = no resolution found
        };

        PeertubeVideoPage(Program *program, std::string server, std::string url, bool autoplay_next) : VideoPage(program, std::move(url)), server(server), autoplay_next(autoplay_next) {}
        const char* get_title() const override { return ""; }
        //BodyItems get_related_media(const std::string &url) override;
        std::string get_download_url(int max_height) override;
        std::string get_video_url(int max_height, bool &has_embedded_audio, std::string &ext) override;
        std::string get_audio_url(std::string &ext) override;
        PluginResult load(std::string &title, std::string &channel_url, std::vector<MediaChapter> &chapters, std::string &err_str) override;
        bool autoplay_next_item() override { return autoplay_next; }
        //void get_subtitles(SubtitleData &subtitle_data) override;
    private:
        std::string server;
        std::vector<VideoSource> video_sources;
        bool autoplay_next;
    };
}
