#pragma once

#include "Page.hpp"

namespace QuickMedia {
    class SaucenaoPage : public LazyFetchPage {
    public:
        SaucenaoPage(Program *program, const std::string &path, bool is_local) : LazyFetchPage(program), path(path), is_local(is_local) {}
        const char* get_title() const override { return "SauceNAO"; }
        bool submit_is_async() const override { return false; }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
    private:
        std::string path;
        bool is_local;
    };
}