#pragma once

#include "Page.hpp"
#include "../include/AsyncTask.hpp"

namespace QuickMedia {
    class SoundcloudPlaylist;

    class SoundcloudPage : public Page {
    public:
        SoundcloudPage(Program *program) : Page(program) {}
        virtual ~SoundcloudPage() = default;
        virtual const char* get_title() const override { return ""; }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
    };

    class SoundcloudSearchPage : public LazyFetchPage {
    public:
        SoundcloudSearchPage(Program *program) : LazyFetchPage(program), submit_page(program) {}
        const char* get_title() const override { return "Search"; }
        bool search_is_filter() override { return false; }
        PluginResult submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) override;
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
        bool lazy_fetch_is_loader() override { return true; }
        void cancel_operation() override;
    private:
        SoundcloudPage submit_page;
        std::string query_urn;
        std::vector<AsyncTask<std::string>> async_download_tasks;
    };

    class SoundcloudUserPage : public SoundcloudPage {
    public:
        SoundcloudUserPage(Program *program, const std::string &username, const std::string &userpage_url, std::string next_href) : SoundcloudPage(program), username(username), userpage_url(userpage_url), next_href(std::move(next_href)), current_page(0) {}
        const char* get_title() const override { return username.c_str(); }
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
    private:
        PluginResult get_continuation_page(BodyItems &result_items);
    private:
        std::string username;
        std::string userpage_url;
        std::string next_href;
        int current_page;
    };

    class SoundcloudPlaylistPage : public SoundcloudPage {
    public:
        SoundcloudPlaylistPage(Program *program, SoundcloudPlaylist *playlist, const std::string &playlist_name) : SoundcloudPage(program), playlist(playlist), playlist_name(playlist_name), track_offset(0) {}
        const char* get_title() const override { return playlist_name.c_str(); }
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
    private:
        SoundcloudPlaylist *playlist;
        std::string playlist_name;
        size_t track_offset;
    };

    class SoundcloudAudioPage : public VideoPage {
    public:
        SoundcloudAudioPage(Program *program, std::string title, const std::string &url, std::string permalink_url) : VideoPage(program, url), title(std::move(title)), permalink_url(std::move(permalink_url)) {}
        const char* get_title() const override { return ""; }
        PluginResult load(std::string &title, std::string &channel_url, std::vector<MediaChapter> &chapters, std::string &err_str) override;
        bool autoplay_next_item() override { return true; }
        std::string url_get_playable_url(const std::string &url) override;
        std::string get_download_url(int max_height) override;
        bool video_should_be_skipped(const std::string &url) override { return url == "track" || url.find("/stream/users/") != std::string::npos; }
    private:
        std::string title;
        // TODO: Remove when youtube-dl is no longer required to download music
        std::string permalink_url;
    };
}