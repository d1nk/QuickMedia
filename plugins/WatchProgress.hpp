#pragma once

#include <stdint.h>
#include <string>
#include <unordered_map>

namespace QuickMedia {
    enum class WatchedStatus {
        WATCHED,
        NOT_WATCHED
    };

    struct WatchProgress {
        int64_t time_pos_sec = 0;
        int64_t duration_sec = 0;
        time_t timestamp = 0;
        std::string thumbnail_url;

        double get_watch_ratio() const;
        bool has_finished_watching() const;
    };

    bool set_watch_progress_for_plugin(const char *plugin_name, const std::string &id, int64_t time_pos_sec, int64_t duration_sec, const std::string &thumbnail_url);
    std::unordered_map<std::string, WatchProgress> get_watch_progress_for_plugin(const char *plugin_name);
    bool toggle_watched_for_plugin_save_to_file(const char *plugin_name, const std::string &id, int64_t duration_sec, const std::string &thumbnail_url, WatchedStatus &watched_status);
}