uniform sampler2D texture;
uniform float radius;
uniform vec2 resolution;

float rounded_rect(vec2 coord, vec2 size, float r) {
	return length(max(abs(coord) - size+r, 0.0)) - r;
}

void main() {
    vec2 uv = gl_TexCoord[0].xy * resolution;
    vec2 center = resolution * 0.5;
    vec2 size = resolution * 0.5;

    vec4 texture_color = texture2D(texture, gl_TexCoord[0].xy);
    float a = clamp(1.0 - smoothstep(0.0, 1.0, rounded_rect(uv - center, size, radius)), 0.0, 1.0);
    texture_color.a *= a;
    gl_FragColor = texture_color;
}