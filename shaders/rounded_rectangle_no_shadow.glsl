uniform float radius;
uniform vec2 resolution;

float rounded_rect(vec2 coord, vec2 size, float r) {
	return length(max(abs(coord) - size+r, 0.0)) - r;
}

void main() {
    vec2 shadow_offset = vec2(20.0, 20.0);
    vec2 uv = gl_TexCoord[0].xy * resolution;
    vec2 center = resolution * 0.5;
    vec2 size = (resolution - shadow_offset * 2.0) * 0.5;

    float rect_dist = rounded_rect(uv - center, size, radius);
    float a = clamp(1.0 - smoothstep(0.0, 1.0, rect_dist), 0.0, 1.0);
    gl_FragColor = vec4(gl_Color.rgb, gl_Color.a * a);
}