#include "../include/BodyItem.hpp"
#include "../include/Theme.hpp"
#include "../include/Config.hpp"

namespace QuickMedia {
    static float floor(float v) {
        return (int)v;
    }

    // static
    std::shared_ptr<BodyItem> BodyItem::create(std::string title, bool selectable) {
        return std::shared_ptr<BodyItem>(new BodyItem(std::move(title), selectable));
    }

    BodyItem::BodyItem(std::string _title, bool selectable) :
        visible(true),
        dirty(false),
        dirty_description(false),
        dirty_author(false),
        dirty_timestamp(false),
        thumbnail_is_local(false),
        userdata(nullptr),
        timestamp(0),
        title_color(get_theme().text_color),
        author_color(get_theme().text_color),
        description_color(get_theme().text_color),
        selectable(selectable)
    {
        if(!_title.empty())
            set_title(std::move(_title));
    }

    BodyItem& BodyItem::operator=(const BodyItem &other) {
        url = other.url;
        thumbnail_url = other.thumbnail_url;
        visible = other.visible;
        dirty = !other.title.empty();
        dirty_description = !other.description.empty();
        dirty_author = !other.author.empty();
        dirty_timestamp = other.timestamp != 0;
        thumbnail_is_local = other.thumbnail_is_local;
        title_text.reset();
        description_text.reset();
        author_text.reset();
        timestamp_text.reset();
        replies_to = other.replies_to;
        replies = other.replies;
        post_number = other.post_number;
        userdata = other.userdata;
        loaded_height = 0.0f;
        loaded_image_size = mgl::vec2f(0.0f, 0.0f);
        loaded_content_height = 0.0f;
        embedded_item_status = other.embedded_item_status;
        if(other.embedded_item) {
            embedded_item.reset(new BodyItem("", true));
            *embedded_item = *other.embedded_item;
        } else {
            embedded_item.reset();
        }
        thumbnail_mask_type = other.thumbnail_mask_type;
        thumbnail_size = other.thumbnail_size;
        reactions.clear();
        for(auto &reaction : other.reactions) {
            Reaction reaction_copy;
            reaction_copy.text = std::make_unique<Text>(*reaction.text);
            reaction_copy.userdata = reaction.userdata;
            reactions.push_back(std::move(reaction_copy));
        }
        title = other.title;
        description = other.description;
        author = other.author;
        timestamp = other.timestamp;
        title_color = other.title_color;
        author_color = other.author_color;
        description_color = other.description_color;
        extra = other.extra;
        keep_alive_frames = other.keep_alive_frames;
        selectable = other.selectable;
        return *this;
    }

    void BodyItem::add_reaction(std::string text, void *userdata) {
        Reaction reaction;
        reaction.text = std::make_unique<Text>(std::move(text), false, floor(get_config().body.reaction_font_size * get_config().scale * get_config().font_scale), 0.0f);
        reaction.userdata = userdata;
        reactions.push_back(std::move(reaction));
    }

    void BodyItem::draw_list(Body *body, mgl::Window &render_target) {

    }
}