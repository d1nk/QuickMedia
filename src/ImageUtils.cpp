#include "../include/ImageUtils.hpp"
#include <string.h>
#include <arpa/inet.h>

namespace QuickMedia {
    static bool gif_get_size(unsigned char *data, size_t data_size, int *width, int *height) {
        if(data_size >= 10 && memcmp(data, "GIF", 3) == 0) {
            *width = ((int)data[7] << 8) | data[6];
            *height = ((int)data[9] << 8) | data[8];
            return true;
        }
        return false;
    }

    static bool png_get_size(unsigned char *data, size_t data_size, int *width, int *height) {
        if(data_size >= 24 && memcmp(data, "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A", 8) == 0) {
            memcpy(width, data + 16, sizeof(int));
            memcpy(height, data + 16 + sizeof(int), sizeof(int));
            *width = htonl(*width);
            *height = htonl(*height);
            return true;
        }
        return false;
    }

#if 0
    static bool is_cpu_little_endian() {
        unsigned short i;
        memcpy(&i, "LE", sizeof(i));
        return (i & 0xF) == 'E';
    }

    static unsigned short read_uint16(unsigned char *data, bool cpu_little_endian, bool file_little_endian) {
        unsigned short result;
        memcpy(&result, data, sizeof(result));
        if(cpu_little_endian != file_little_endian)
            result = __builtin_bswap16(result);
        return result;
    }

    static unsigned int read_uint32(unsigned char *data, bool cpu_little_endian, bool file_little_endian) {
        unsigned int result;
        memcpy(&result, data, sizeof(result));
        if(cpu_little_endian != file_little_endian)
            result = __builtin_bswap32(result);
        return result;
    }
#endif
#if 0
    static bool tiff_get_size(unsigned char *data, size_t data_size, int *width, int *height) {
        if(data_size < 8)
            return false;

        bool cpu_little_endian = is_cpu_little_endian();
        bool file_little_endian = true;
        if(memcmp(data, "II", 2) == 0)
            file_little_endian = true;
        else if(memcmp(data, "MM", 2) == 0)
            file_little_endian = false;
        else
            return false;

        unsigned short id = read_uint16(data, cpu_little_endian, file_little_endian);
        if(id != 42)
            return false;

        unsigned int offset_to_ifd = read_uint32(data, cpu_little_endian, file_little_endian);
        if(offset_to_ifd )
    }
#endif

    static bool jpeg_get_size(unsigned char *data, size_t data_size, int *width, int *height) {
        if(data_size < 11 || memcmp(data, "\xFF\xD8\xFF\xE0", 4) != 0)
            return false;

        unsigned short block_length = ((int)data[4] << 8) | data[5];
        if(memcmp(data + 6, "JFIF\0", 5) != 0)
            return false;

        size_t index = 4;
        while(index < data_size) {
            index += block_length;
            if(index + 1 >= data_size) return false;
            if(data[index] != 0xFF) return false; // Check if start of block
            if((data[index + 1] == 0xC0 || data[index + 1] == 0xC2) && index + 8 < data_size) { // Start of frame marker
                *height = ((int)data[index + 5] << 8) | data[index + 6];
                *width = ((int)data[index + 7] << 8) | data[index + 8];
                return true;
            } else {
                if(index + 2 >= data_size) return false;
                index += 2;
                block_length = ((int)data[index] << 8) | data[index + 1];
            }
        }

        return false;
    }

    // TODO: Also support exif files. Exif files are containers for jpeg, tiff etc so once tiff has been implemented so can exif.
    // Exif header is similar to jpeg.
    bool image_get_resolution(const Path &path, int *width, int *height, ImageType *image_type) {
        FILE *file = fopen(path.data.c_str(), "rb");
        if(!file)
            return false;

        unsigned char data[512];
        size_t bytes_read = fread(data, 1, sizeof(data), file);
        fclose(file);

        if(png_get_size(data, bytes_read, width, height)) {
            if(image_type)
                *image_type = ImageType::PNG;
            return true;
        } else if(gif_get_size(data, bytes_read, width, height)) {
            if(image_type)
                *image_type = ImageType::GIF;
            return true;
        } else if(jpeg_get_size(data, bytes_read, width, height)) {
            if(image_type)
                *image_type = ImageType::JPG;
            return true;
        }
        return false;
    }
}