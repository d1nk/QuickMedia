#include "../include/Json.hpp"
#include <rapidjson/document.h>

namespace QuickMedia {
    static rapidjson::Value nullValue(rapidjson::kNullType);
    const rapidjson::Value& GetMember(const rapidjson::Value &obj, const char *key) {
        auto it = obj.FindMember(key);
        if(it != obj.MemberEnd())
            return it->value;
        return nullValue;
    }

    DownloadResult download_json(rapidjson::Document &result, const std::string &url, std::vector<CommandArg> additional_args, bool use_browser_useragent, std::string *err_msg) {
        if(download_to_json(url, result, std::move(additional_args), use_browser_useragent, err_msg == nullptr) != DownloadResult::OK) {
            // Cant get error since we parse directly to json. TODO: Make this work somehow?
            if(err_msg)
                *err_msg = "Failed to download/parse json";
            return DownloadResult::NET_ERR;
        }
        return DownloadResult::OK;
    }
}