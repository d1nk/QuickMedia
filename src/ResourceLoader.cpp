#include "../include/ResourceLoader.hpp"
#include "../include/Program.hpp"
#include "../include/Path.hpp"
#include "../include/StringUtils.hpp"
#include <mglpp/graphics/Font.hpp>
#include <mglpp/graphics/Texture.hpp>
#include <mglpp/system/MemoryMappedFile.hpp>
#include <array>
#include <unordered_map>
#include <malloc.h>
#include <assert.h>

static std::string resource_root;
static std::array<std::unique_ptr<mgl::MemoryMappedFile>, 4> font_file_cache;
// font_cache[(unsigned int)font_type][character_size]
static std::array<std::unordered_map<unsigned int, std::unique_ptr<mgl::Font>>, 4> font_cache;
static std::unordered_map<std::string, std::unique_ptr<mgl::Texture>> texture_cache;
static bool use_system_fonts = false;

namespace QuickMedia {
    void set_resource_loader_root_path(const char *new_resource_root) {
        resource_root = new_resource_root;
    }

    const char* get_resource_loader_root_path() {
        return resource_root.c_str();
    }

    void set_use_system_fonts(bool use) {
        use_system_fonts = use;
    }
}

namespace QuickMedia::FontLoader {
    static int accumulate_string(char *data, int size, void *userdata) {
        std::string *str = (std::string*)userdata;
        if(str->size() + size > 1024 * 1024 * 100) // 100mb sane limit, TODO: make configurable
            return 1;
        str->append(data, size);
        return 0;
    }

    mgl::Font* get_font(FontType font_type, unsigned int character_size) {
        // Make mgl font size match sfml font size
        character_size += 5;

        mgl::MemoryMappedFile *mapped_file = font_file_cache[(unsigned int)font_type].get();
        if(!mapped_file) {
            std::vector<std::string> noto_directories;
            std::string font_file_name;
            switch(font_type) {
                case FontType::LATIN: {
                    std::string output;
                    const char *args[] = { "fc-match", "sans:lang=en", "file", nullptr };
                    if(use_system_fonts && exec_program(args, accumulate_string, &output) == 0 && output.size() > 6) {
                        Path path = strip(output.substr(6));
                        noto_directories.push_back(path.parent().data);
                        font_file_name = path.filename();
                    } else {
                        noto_directories.push_back("/usr/share/fonts/noto");
                        noto_directories.push_back("/usr/share/fonts/truetype/noto");
                        font_file_name = "NotoSans-Regular.ttf";
                    }
                    break;
                }
                case FontType::LATIN_BOLD: {
                    std::string output;
                    const char *args[] = { "fc-match", "sans:bold:lang=en", "file", nullptr };
                    if(use_system_fonts && exec_program(args, accumulate_string, &output) == 0 && output.size() > 6) {
                        Path path = strip(output.substr(6));
                        noto_directories.push_back(path.parent().data);
                        font_file_name = path.filename();
                    } else {
                        noto_directories.push_back("/usr/share/fonts/noto");
                        noto_directories.push_back("/usr/share/fonts/truetype/noto");
                        font_file_name = "NotoSans-Bold.ttf";
                    }
                    break;
                }
                case FontType::CJK: {
                    std::string output;
                    const char *args[] = { "fc-match", "sans:lang=ja", "file", nullptr };
                    if(use_system_fonts && exec_program(args, accumulate_string, &output) == 0 && output.size() > 6) {
                        Path path = strip(output.substr(6));
                        noto_directories.push_back(path.parent().data);
                        font_file_name = path.filename();
                    } else {
                        noto_directories.push_back("/usr/share/fonts/noto-cjk");
                        noto_directories.push_back("/usr/share/fonts/truetype/noto-cjk");
                        font_file_name = "NotoSansCJK-Regular.ttc";
                    }
                    break;
                }
                case FontType::SYMBOLS: {
                    // TODO: Allow changing with system font setting
                    noto_directories.push_back("/usr/share/fonts/noto");
                    noto_directories.push_back("/usr/share/fonts/truetype/noto");
                    font_file_name = "NotoSansSymbols2-Regular.ttf";
                    break;
                }
            }

            bool successfully_loaded_file = false;
            auto new_memory_mapped_file = std::make_unique<mgl::MemoryMappedFile>();
            for(const std::string &noto_dir : noto_directories) {
                if(new_memory_mapped_file->load((noto_dir + "/" + font_file_name).c_str(), mgl::MemoryMappedFile::LoadOptions{true, false})) {
                    successfully_loaded_file = true;
                    break;
                }
            }

            if(!successfully_loaded_file) {
                fprintf(stderr, "Warning: Failed to load font file: %s\n", font_file_name.c_str());
            }

            mapped_file = new_memory_mapped_file.get();
            font_file_cache[(unsigned int)font_type] = std::move(new_memory_mapped_file);
        }

        if(!mapped_file->data()) {
            return nullptr;
        }

        mgl::Font *font = font_cache[(unsigned int)font_type][character_size].get();
        if(!font) {
            auto new_font = std::make_unique<mgl::Font>();
            if(!new_font->load_from_file(*mapped_file, character_size)) {
                fprintf(stderr, "Warning: Failed to load font at character size %u\n", character_size);
            }

            font = new_font.get();
            font_cache[(unsigned int)font_type][character_size] = std::move(new_font);
        }
        return font;
    }
}

namespace QuickMedia::TextureLoader {
    mgl::Texture* get_texture(const char *filepath, bool pixel_coordinates) {
        assert(!resource_root.empty());
        std::string str = filepath;
        auto it = texture_cache.find(str);
        if(it != texture_cache.end())
            return it->second.get();
        
        auto new_texture = std::make_unique<mgl::Texture>();
        mgl::Texture *result = new_texture.get();
        if(!new_texture->load_from_file((resource_root + str).c_str(), mgl::Texture::LoadOptions{ false, pixel_coordinates }))
            fprintf(stderr, "Failed to load image: %s%s\n", resource_root.c_str(), filepath);

        texture_cache[str] = std::move(new_texture);
        malloc_trim(0);
        return result;
    }
}
