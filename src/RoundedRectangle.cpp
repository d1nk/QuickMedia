#include "../include/RoundedRectangle.hpp"
#include <mglpp/graphics/Shader.hpp>
#include <mglpp/window/Window.hpp>
#include <assert.h>

namespace QuickMedia {
    static const float shadow_radius = 20.0f; // Has to match the shadow offset in rounded_rectangle.glsl
    static float cached_radius = 0.0f;
    static float cached_resolution_x = 0.0f;
    static float cached_resolution_y = 0.0f;

    static float abs(float value) {
        return value >= 0.0f ? value : -value;
    }

    RoundedRectangle::RoundedRectangle(mgl::vec2f size, float radius, mgl::Color color, mgl::Shader *rounded_rectangle_shader) :
        radius(radius), pos(0.0f, 0.0f), size(size), rounded_rectangle_shader(rounded_rectangle_shader)
    {
        assert(rounded_rectangle_shader);
        set_color(color);
        vertices[0].texcoords = mgl::vec2f(0.0f, 0.0f);
        vertices[1].texcoords = mgl::vec2f(1.0f, 0.0f);
        vertices[2].texcoords = mgl::vec2f(1.0f, 1.0f);
        vertices[3].texcoords = mgl::vec2f(0.0f, 1.0f);
    }

    void RoundedRectangle::set_position(mgl::vec2f pos) {
        this->pos = pos;
        const float shadow_radius_ins = rounded_rectangle_shader->is_valid() ? shadow_radius : 0.0f;
        vertices[0].position = pos + mgl::vec2f(-shadow_radius_ins, -shadow_radius_ins);
        vertices[1].position = pos + mgl::vec2f(shadow_radius_ins, -shadow_radius_ins) + mgl::vec2f(size.x, 0.0f);
        vertices[2].position = pos + mgl::vec2f(shadow_radius_ins, shadow_radius_ins) + mgl::vec2f(size.x, size.y);
        vertices[3].position = pos + mgl::vec2f(-shadow_radius_ins, shadow_radius_ins) + mgl::vec2f(0.0f, size.y);
    }

    void RoundedRectangle::set_size(mgl::vec2f size) {
        this->size = size;
        set_position(pos);
    }

    void RoundedRectangle::set_color(mgl::Color color) {
        for(size_t i = 0; i < 4; ++i)
            vertices[i].color = color;
    }

    mgl::vec2f RoundedRectangle::get_position() const {
        return pos;
    }

    mgl::vec2f RoundedRectangle::get_size() const {
        return size;
    }

    void RoundedRectangle::draw(mgl::Window &target) {
        const mgl::vec2f resolution = size + mgl::vec2f(shadow_radius*2.0f, shadow_radius*2.0f);

        if(abs(cached_radius - radius) > 0.01f) {
            // TODO: Remove these for optimizations. Also do the same in other places where set_uniform is called
            rounded_rectangle_shader->set_uniform("radius", radius);
            cached_radius = radius;
        }

        if(abs(cached_resolution_x - resolution.x) > 0.01f || abs(cached_resolution_y - resolution.y) > 0.01f) {
            rounded_rectangle_shader->set_uniform("resolution", size + mgl::vec2f(shadow_radius*2.0f, shadow_radius*2.0f));
            cached_resolution_x = resolution.x;
            cached_resolution_y = resolution.y;
        }
        
        target.draw(vertices, 4, mgl::PrimitiveType::Quads, rounded_rectangle_shader);
    }
}