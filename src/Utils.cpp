#include "../include/Utils.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>

namespace QuickMedia {
    static bool qm_enable_touch = false;
    static bool qm_enable_touch_set = false;
    static bool wayland_display_set = false;
    static const char *wayland_display = nullptr;

    void show_virtual_keyboard() {
        if(!is_touch_enabled())
            return;

        fprintf(stderr, "Show virtual keyboard\n");
        system("busctl call --user sm.puri.OSK0 /sm/puri/OSK0 sm.puri.OSK0 SetVisible b true");
    }

    void hide_virtual_keyboard() {
        if(!is_touch_enabled())
            return;

        fprintf(stderr, "Hide virtual keyboard\n");
        system("busctl call --user sm.puri.OSK0 /sm/puri/OSK0 sm.puri.OSK0 SetVisible b false");
    }

    bool is_touch_enabled() {
        if(qm_enable_touch_set)
            return qm_enable_touch;

        const char *qm_enable_touch_env = getenv("QM_ENABLE_TOUCH");
        if(qm_enable_touch_env && qm_enable_touch_env[0] == '1')
            qm_enable_touch = true;

        qm_enable_touch_set = true;
        return qm_enable_touch;
    }

    // TODO: Find a better way to detect this. This will return true on ubuntu when running gnome in x11 mode
    bool is_running_wayland() {
        if(wayland_display_set)
            return wayland_display;
        
        wayland_display = getenv("WAYLAND_DISPLAY");
        wayland_display_set = true;
        return wayland_display;
    }

    time_t iso_utc_to_unix_time(const char *time_str) {
        int year = 0;
        int month = 0;
        int day = 0;
        int hour = 0;
        int minute = 0;
        int second = 0;
        // TODO: Handle timezone
        sscanf(time_str, "%d-%d-%dT%d:%d:%d", &year, &month, &day, &hour, &minute, &second);
        if(year == 0) return 0;
        
        struct tm time;
        memset(&time, 0, sizeof(time));
        time.tm_year = year - 1900;
        time.tm_mon = month - 1;
        time.tm_mday = day;
        time.tm_hour = hour;
        time.tm_min = minute;
        time.tm_sec = second;
        return timegm(&time);
    }

    std::string unix_time_to_local_time_str(time_t unix_time) {
        struct tm time_tm;
        localtime_r(&unix_time, &time_tm);
        char time_str[128] = {0};
        strftime(time_str, sizeof(time_str) - 1, "%Y %b %d, %a %H:%M", &time_tm);
        return time_str;
    }

    int64_t get_boottime_milliseconds() {
        struct timespec time;
        if(clock_gettime(CLOCK_BOOTTIME, &time) == -1 && errno == EINVAL)
            clock_gettime(CLOCK_MONOTONIC, &time);
        return (int64_t)time.tv_sec * 1000 + (int64_t)time.tv_nsec / 1000000;
    }

    mgl::vec2f vec2f_floor(float x, float y) {
        return mgl::vec2f(int(x), int(y));
    }
}
