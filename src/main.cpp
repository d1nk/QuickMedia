#include "../include/QuickMedia.hpp"
#include <unistd.h>

int main(int argc, char **argv) {
    XInitThreads();
    setlocale(LC_ALL, "C"); // Sigh... stupid C
    QuickMedia::Program program;
    return program.run(argc, argv);
}
