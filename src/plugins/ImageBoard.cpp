#include "../../plugins/ImageBoard.hpp"

namespace QuickMedia {
    void ImageBoardThreadPage::copy_to_clipboard(const BodyItem *body_item) {
        set_clipboard(body_item->get_title());
    }

    PluginResult ImageBoardThreadPage::login(const std::string &token, const std::string &pin, std::string &response_msg) {
        (void)token;
        (void)pin;
        response_msg = "Login is not supported on this image board";
        return PluginResult::ERR;
    }

    const std::string& ImageBoardThreadPage::get_pass_id() {
        static std::string empty_str;
        return empty_str;
    }
}