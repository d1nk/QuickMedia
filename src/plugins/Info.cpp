#include "../../plugins/Info.hpp"
#include "../../plugins/Saucenao.hpp"
#include "../../plugins/Youtube.hpp"
#include "../../include/StringUtils.hpp"
#include "../../include/Program.hpp"
#include "../../include/Notification.hpp"
#include "../../include/Storage.hpp"

namespace QuickMedia {
    static const char *REVERSE_IMAGE_SEARCH_URL = "reverse-image-search://";
    static const char *GOOGLE_SEARCH_URL = "google-search://";

    static bool is_youtube_url(const std::string &url) {
        return url.find("youtube.com/") != std::string::npos || url.find("youtu.be/") != std::string::npos;
    }

    static PluginResult open_with_browser(const std::string &url) {
        const char *launch_program = "xdg-open";
        if(!is_program_executable_by_name("xdg-open")) {
            launch_program = getenv("BROWSER");
            if(!launch_program) {
                show_notification("QuickMedia", "xdg-utils which provides xdg-open needs to be installed to open urls. Alternatively set the $BROWSER environment variable to a browser", Urgency::CRITICAL);
                return PluginResult::ERR;
            }
        }

        std::string url_modified = url;
        if(strncmp(url.c_str(), "http://", 7) != 0 && strncmp(url.c_str(), "https://", 8) != 0)
            url_modified = "https://" + url;

        const char *args[] = { launch_program, url_modified.c_str(), nullptr };
        return exec_program_async(args, nullptr) == 0 ? PluginResult::OK : PluginResult::ERR;
    }

    PluginResult InfoPage::submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) {
        if(string_starts_with(args.url, REVERSE_IMAGE_SEARCH_URL)) {
            std::string image_url = args.url.substr(strlen(REVERSE_IMAGE_SEARCH_URL));
            result_tabs.push_back(Tab{create_body(), std::make_unique<SaucenaoPage>(program, image_url, false), nullptr});
            return PluginResult::OK;
        } else if(string_starts_with(args.url, GOOGLE_SEARCH_URL)) {
            const std::string search_term = args.url.substr(strlen(GOOGLE_SEARCH_URL));
            const std::string search_url = "https://www.google.com/search?q=" + url_param_encode(search_term);
            return open_with_browser(search_url);
        } else if(is_youtube_url(args.url)) {
            result_tabs.push_back(Tab{nullptr, std::make_unique<YoutubeVideoPage>(program, args.url), nullptr});
            return PluginResult::OK;
        } else {
            return open_with_browser(args.url);
        }
    }

    void InfoPage::copy_to_clipboard(const BodyItem *body_item) {
        std::string url;
        if(string_starts_with(body_item->url, REVERSE_IMAGE_SEARCH_URL)) {
            url = body_item->url.substr(strlen(REVERSE_IMAGE_SEARCH_URL));
        } else if(string_starts_with(body_item->url, GOOGLE_SEARCH_URL)) {
            url = body_item->url.substr(strlen(GOOGLE_SEARCH_URL));
        } else {
            url = body_item->url;
        }

        if(!url.empty())
            set_clipboard(url);
    }

    // static
    std::shared_ptr<BodyItem> InfoPage::add_url(const std::string &url) {
        std::string title;
        if(is_youtube_url(url))
            title = "Play " + url;
        else
            title = "Open " + url + " in a browser";
        auto body_item = BodyItem::create(std::move(title));
        body_item->url = url;
        return body_item;
    }

    // static
    std::shared_ptr<BodyItem> InfoPage::add_reverse_image_search(const std::string &image_url) {
        auto body_item = BodyItem::create("Reverse search the image with SauceNAO");
        body_item->url = REVERSE_IMAGE_SEARCH_URL + image_url;
        return body_item;
    }

    // static
    std::shared_ptr<BodyItem> InfoPage::add_google_search(const std::string &search_term) {
        auto body_item = BodyItem::create("Search for \"" + search_term + "\" with google search");
        body_item->url = GOOGLE_SEARCH_URL + search_term;
        return body_item;
    }
}