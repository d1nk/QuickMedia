#include "../../plugins/Manga.hpp"
#include "../../include/Program.hpp"
#include "../../include/Notification.hpp"

namespace QuickMedia {
    TrackResult MangaChaptersPage::track(const std::string &str) {
        const char *args[] = { "automedia", "add", "html", content_url.data(), "--start-after", str.data(), "--name", content_title.data(), nullptr };
        if(exec_program(args, nullptr, nullptr) == 0) {
            show_notification("QuickMedia", "You are now tracking \"" + content_title + "\" after \"" + str + "\"", Urgency::LOW);
            return TrackResult::OK;
        } else {
            show_notification("QuickMedia", "Failed to track media \"" + content_title + "\", chapter: \"" + str + "\"", Urgency::CRITICAL);
            return TrackResult::ERR;
        }
    }

    void MangaChaptersPage::on_navigate_to_page(Body *body) {
        (void)body;
        std::string manga_id;
        if(extract_id_from_url(content_url, manga_id))
            load_manga_content_storage(get_service_name(), content_title, content_url, manga_id);
    }

    std::shared_ptr<BodyItem> MangaChaptersPage::get_bookmark_body_item(BodyItem*) {
        auto body_item = BodyItem::create(content_title);
        body_item->url = content_url;
        body_item->thumbnail_url = thumbnail_url;
        return body_item;
    }
}