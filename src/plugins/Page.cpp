#include "../../plugins/Page.hpp"
#include "../../include/StringUtils.hpp"
#include "../../include/Theme.hpp"
#include "../../include/Storage.hpp"
#include "../../include/QuickMedia.hpp"
#include <json/reader.h>

namespace QuickMedia {
    DownloadResult Page::download_json(Json::Value &result, const std::string &url, std::vector<CommandArg> additional_args, bool use_browser_useragent, std::string *err_msg) {
        std::string server_response;
        if(download_to_string(url, server_response, std::move(additional_args), use_browser_useragent, err_msg == nullptr) != DownloadResult::OK) {
            if(err_msg)
                *err_msg = server_response;
            return DownloadResult::NET_ERR;
        }

        if(server_response.empty()) {
            result = Json::Value::nullSingleton();
            return DownloadResult::OK;
        }

        Json::CharReaderBuilder json_builder;
        std::unique_ptr<Json::CharReader> json_reader(json_builder.newCharReader());
        std::string json_errors;
        if(!json_reader->parse(&server_response[0], &server_response[server_response.size()], &result, &json_errors)) {
            fprintf(stderr, "download_json error: %s\n", json_errors.c_str());
            if(err_msg)
                *err_msg = std::move(json_errors);
            return DownloadResult::ERR;
        }

        return DownloadResult::OK;
    }

    std::unique_ptr<Body> Page::create_body(bool plain_text_list, bool prefer_card_view) {
        return program->create_body(plain_text_list, prefer_card_view);
    }

    std::unique_ptr<SearchBar> Page::create_search_bar(const std::string &placeholder_text, int search_delay) {
        return program->create_search_bar(placeholder_text, search_delay);
    }

    bool Page::load_manga_content_storage(const char *service_name, const std::string &manga_title, const std::string &manga_url, const std::string &manga_id) {
        return program->load_manga_content_storage(service_name, manga_title, manga_url, manga_id);
    }

    void Page::set_clipboard(const std::string &str) {
        program->set_clipboard(str);
    }

    void Page::copy_to_clipboard(const BodyItem *body_item) {
        std::string title = body_item->get_title();
        std::string author = body_item->get_author();
        std::string description = body_item->get_description();

        std::string clipboard = std::move(title);

        if(!author.empty()) {
            if(!clipboard.empty())
                clipboard += '\n';
            clipboard += std::move(author);
        }
        
        if(!description.empty()) {
            if(!clipboard.empty())
                clipboard += '\n';
            clipboard += std::move(description);
        }

        if(!clipboard.empty())
            program->set_clipboard(clipboard);
    }

    PluginResult BookmarksPage::submit(const SubmitArgs &args, std::vector<Tab> &result_tabs) {
        return redirect_page->submit(args, result_tabs);
    }

    PluginResult BookmarksPage::lazy_fetch(BodyItems &result_items) {
        const char *bookmark_name = redirect_page->get_bookmark_name();
        if(!bookmark_name)
            return PluginResult::ERR;

        Path bookmark_path = get_storage_dir().join("bookmarks").join(bookmark_name);
        Json::Value json_root;
        if(!read_file_as_json(bookmark_path, json_root) || !json_root.isArray())
            return PluginResult::OK;

        std::vector<const Json::Value*> bookmark_json_items;
        for(const Json::Value &item : json_root) {
            if(!item.isObject())
                continue;

            const Json::Value &timestamp = item["timestamp"];
            if(!timestamp.isInt64())
                continue;

            bookmark_json_items.push_back(&item);
        }

        std::sort(bookmark_json_items.begin(), bookmark_json_items.end(), [](const Json::Value *val1, const Json::Value *val2) {
            const Json::Value &timestamp1 = (*val1)["timestamp"];
            const Json::Value &timestamp2 = (*val2)["timestamp"];
            return timestamp1.asInt64() > timestamp2.asInt64();
        });

        const time_t time_now = time(nullptr);
        for(const Json::Value *item_json : bookmark_json_items) {
            if(!item_json->isObject())
                continue;

            const Json::Value &title_json = (*item_json)["title"];
            const Json::Value &author_json = (*item_json)["author"];
            const Json::Value &url_json = (*item_json)["url"];
            const Json::Value &thumbnail_url_json = (*item_json)["thumbnail_url"];
            const Json::Value &timestamp_json = (*item_json)["timestamp"];

            auto body_item = BodyItem::create(title_json.isString() ? title_json.asString() : "");
            if(author_json.isString())
                body_item->set_author(author_json.asString());

            if(url_json.isString())
                body_item->url = url_json.asString();

            if(thumbnail_url_json.isString()) {
                body_item->thumbnail_url = thumbnail_url_json.asString();
                body_item->thumbnail_size = thumbnail_size;
                body_item->thumbnail_is_local = local_thumbnail;
            }

            if(timestamp_json.isInt64()) {
                body_item->set_description("Bookmarked " + seconds_to_relative_time_str(time_now - timestamp_json.asInt64()));
                body_item->set_description_color(get_theme().faded_text_color);
            }

            result_items.push_back(std::move(body_item));
        }

        return PluginResult::OK;
    }
}