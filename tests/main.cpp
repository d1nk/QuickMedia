#include <stdio.h>
#include "../include/NetUtils.hpp"
#include "../plugins/EpisodeNameParser.hpp"

#define assert_fail(str) do { fprintf(stderr, "Assert failed on line %d, reason: %s\n", __LINE__, (str)); exit(1); } while(0)
#define assert_equals(a, b) do { if((a) != (b)) { fprintf(stderr, "Assert failed on line %d, %s == %s\n", __LINE__, #a, #b); exit(1); } } while(0)

using namespace QuickMedia;

int main() {
    std::vector<std::string> urls;
    const char *str;

    str = "example.com";
    urls = ranges_get_strings(str, extract_urls(str));
    assert_equals(urls.size(), 1);
    assert_equals(urls[0], "example.com");

    str = "example.com, is where I like to go";
    urls = ranges_get_strings(str, extract_urls(str));
    assert_equals(urls.size(), 1);
    assert_equals(urls[0], "example.com");

    str = "The website I like to go to is example.com";
    urls = ranges_get_strings(str, extract_urls(str));
    assert_equals(urls.size(), 1);
    assert_equals(urls[0], "example.com");

    str = "example.com. Is also a website";
    urls = ranges_get_strings(str, extract_urls(str));
    assert_equals(urls.size(), 1);
    assert_equals(urls[0], "example.com");

    str = "example.com: the best test website";
    urls = ranges_get_strings(str, extract_urls(str));
    assert_equals(urls.size(), 1);
    assert_equals(urls[0], "example.com");

    str = "is it example.com? or not?";
    urls = ranges_get_strings(str, extract_urls(str));
    assert_equals(urls.size(), 1);
    assert_equals(urls[0], "example.com");

    str = "these. are. not. websites.";
    urls = ranges_get_strings(str, extract_urls(str));
    assert_equals(urls.size(), 0);

    str = "This is not an url: example.";
    urls = ranges_get_strings(str, extract_urls(str));
    assert_equals(urls.size(), 0);

    str = "the.se/~#423-_/2f.no/3df a.re considered sub.websit.es, this.is.not";
    urls = ranges_get_strings(str, extract_urls(str));
    assert_equals(urls.size(), 3);
    assert_equals(urls[0], "the.se/~#423-_/2f.no/3df");
    assert_equals(urls[1], "a.re");
    assert_equals(urls[2], "sub.websit.es");

    str = "(see https://emojipedia.org/emoji/%23%EF%B8%8F%E2%83%A3/)";
    urls = ranges_get_strings(str, extract_urls(str));
    assert_equals(urls.size(), 1);
    assert_equals(urls[0], "https://emojipedia.org/emoji/%23%EF%B8%8F%E2%83%A3/");

    str = "[sneed](https://sneedville.com)";
    urls = ranges_get_strings(str, extract_urls(str));
    assert_equals(urls.size(), 1);
    assert_equals(urls[0], "https://sneedville.com");

    std::string html_unescaped_str = "hello &#039; world";
    html_unescape_sequences(html_unescaped_str);
    assert_equals(html_unescaped_str, "hello ' world");

    html_unescaped_str = "hello &#x27; world";
    html_unescape_sequences(html_unescaped_str);
    assert_equals(html_unescaped_str, "hello ' world");

    std::optional<EpisodeNameParts> name_parts1 = episode_name_extract_parts("[SubsPlease] Shikkakumon no Saikyou Kenja - 07 (1080p) [83EFD76A].mkv");
    assert_equals(name_parts1.has_value(), true);
    assert_equals(name_parts1->group, "SubsPlease");
    assert_equals(name_parts1->anime, "Shikkakumon no Saikyou Kenja");
    assert_equals(name_parts1->season.size(), 0);
    assert_equals(name_parts1->episode, "07");

    std::optional<EpisodeNameParts> name_parts2 = episode_name_extract_parts("[SubsPlease] Shingeki no Kyojin (The Final Season) - 81 (1080p) [601A33BD].mkv");
    assert_equals(name_parts2.has_value(), true);
    assert_equals(name_parts2->group, "SubsPlease");
    assert_equals(name_parts2->anime, "Shingeki no Kyojin (The Final Season)");
    assert_equals(name_parts2->season.size(), 0);
    assert_equals(name_parts2->episode, "81");

    std::optional<EpisodeNameParts> name_parts3 = episode_name_extract_parts("[SubsPlease] Lupin III - Part 6 - 18 (1080p) [98204042].mkv");
    assert_equals(name_parts3.has_value(), true);
    assert_equals(name_parts3->group, "SubsPlease");
    assert_equals(name_parts3->anime, "Lupin III");
    assert_equals(name_parts3->season, "Part 6");
    assert_equals(name_parts3->episode, "18");

    std::optional<EpisodeNameParts> name_parts4 = episode_name_extract_parts("[SubsPlease] Kimetsu no Yaiba - Yuukaku-hen - 11 (1080p) [BE15F231].mkv");
    assert_equals(name_parts4.has_value(), true);
    assert_equals(name_parts4->group, "SubsPlease");
    assert_equals(name_parts4->anime, "Kimetsu no Yaiba");
    assert_equals(name_parts4->season, "Yuukaku-hen");
    assert_equals(name_parts4->episode, "11");

    return 0;
}
