# QuickMedia Video Player
The video player internally used by QuickMedia. Uses libmpv.\
The video player window is embedded inside QuickMedia and QuickMedia and this video player communicate over a file descriptor (socketpair) using json (json without newline formatting; one command per line).
# IPC commands
## time-pos
Return seeking position in file in seconds
### request
```
{
    "command": "time-pos",
    "request_id": 232                       // Optional
}
```
### response on success
```
{
    "status": "success",
    "data": 112.432,
    "request_id": 232,                      // Optional. Its provided if request_id was provided in the request
}
```
### response on error
```
{
    "status": "error",
    "message": "error message",
    "request_id": 233                       // Optional. Its provided if request_id was provided in the request
}
```
## sub-add
Add a subtitle file/url that is loaded asynchronously
### request
```
{
    "command": "sub-add",
    "data": {
        "file": "path/to/file/or/url",
        "title": "title",                   // Optional
        "language": "en_us"                 // Optional
    },
    "request_id": 233                       // Optional
}
```
### response on success
```
{
    "status": "success",
    "request_id": 233                       // Optional. Its provided if request_id was provided in the request
}
```
### response on error
```
{
    "status": "error",
    "message": "error message",
    "request_id": 233                       // Optional. Its provided if request_id was provided in the request
}
```
# IPC event
```
{
    "event": "file-loaded"
}
```