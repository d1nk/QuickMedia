#pragma once

#include <unistd.h>
#include <errno.h>

static ssize_t read_eintr(int fd, void *buffer, size_t size) {
    while(true) {
        ssize_t bytes_read = read(fd, buffer, size);
        if(bytes_read == -1) {
            if(errno != EINTR)
                return -1;
        } else {
            return bytes_read;
        }
    }
}

static ssize_t write_all(int fd, const void *buffer, size_t size) {
    ssize_t bytes_written = 0;
    while((size_t)bytes_written < size) {
        ssize_t written = write(fd, (char*)buffer + bytes_written, size - bytes_written);
        if(written == -1) {
            if(errno != EINTR)
                return -1;
        } else {
            bytes_written += written;
        }
    }
    return bytes_written;
}